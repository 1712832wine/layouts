/* GiftCard */
let webPaymentForm = {
    formPaymentNew: true,
    formPaymentID: 'form#formPayment',
    boxRecipient: '#boxRecipient',
    boxPaymentItems: '#boxPaymentItems',
    boxCart: '#boxCart',
    boxPreview: '#boxPreview',

    init: function (isPaymentNew, dataForm_JsonString) {
        let _self = this;

        // Payment is new
        _self.formPaymentNew = (typeof isPaymentNew !== 'undefined' && isPaymentNew);

        // Events
        _self.setEvents();

        // Data Form
        _self.setDataForm(dataForm_JsonString);
    },

    setEvents: function () {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        let boxPaymentItemsObj = $(_self.boxPaymentItems);
        let boxRecipientObj = $(_self.boxRecipient);

        // Validate
        formPaymentObj.validate({
            submit: {
                settings: {
                    clear: 'keypress',
                    display: "inline",
                    button: "[type='submit']",
                    inputContainer: 'form-group',
                    errorListClass: 'form-tooltip-error',
                },
                callback: {
                    onSubmit: function (node, formdata) {
                        /* Deny duplicate click && Clears all form errors */
                        formPaymentObj.find('.btn_payment').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');
                        formPaymentObj.removeError();

                        if (_self.formPaymentNew) {
                            let isValidate = true;

                            /* Check price */
                            let item = {};
                            let customPriceObj = formPaymentObj.find('[name="custom_price"]');

                            item.price = parseFloat(formdata['custom_price']);
                            item.price = (!isNaN(item.price) && item.price > 0) ? item.price : 0;

                            item.price_min = parseFloat(customPriceObj.attr('data-price_min'));
                            item.price_min = (!isNaN(item.price_min) && item.price_min > 0) ? item.price_min : 0;

                            item.price_max = parseFloat(customPriceObj.attr('data-price_max'));
                            item.price_max = (!isNaN(item.price_max) && item.price_max > 0) ? item.price_max : 0;

                            if (!(item.price_min <= item.price && item.price <= item.price_max)) {
                                isValidate = false;
                                formPaymentObj.addError({
                                    'custom_price': `Accept Amount From ${item.price_min} to ${item.price_max}`,
                                });
                            }

                            if (!isValidate) {
                                formPaymentObj.find('.btn_payment').prop('disabled', false).removeAttr('disabled').removeClass('disabled');
                                let errorElement = formPaymentObj.find('.error').first();
                                scrollJumpto(errorElement.length > 0 ? errorElement : formPaymentObj, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
                                return false;
                            }
                        }

                        waitingDialog.show("Please wait a moment ...");
                        node[0].submit();
                    },
                    onError: function () {
                        let errorElement = formPaymentObj.find('.error').first();
                        scrollJumpto(errorElement.length > 0 ? errorElement : formPaymentObj, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
                    }
                }
            }
        });

        // Events
        formPaymentObj.on('click', 'input[name="send_to_friend"]', function () {
            let _this = $(this);
            let sendToFriend = _this.is(':checked');
            if (sendToFriend) {
                _this.prop('checked', true);
                boxRecipientObj.show();
            } else {
                _this.prop('checked', false);
                boxRecipientObj.hide();
            }
        });

        if (_self.formPaymentNew) {
            boxPaymentItemsObj.on('click', '.paymentItem', function (e) {
                e.preventDefault();
                let _this = $(this);

                /* Init active */
                boxPaymentItemsObj.find('.paymentItem').removeClass('active');
                _this.addClass('active');

                /* Get Item */
                let item = {};
                item.id = parseInt(_this.attr('data-id'));
                item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

                item.name = _this.attr('data-name');
                item.image = _this.attr('data-image');

                item.price = parseFloat(_this.attr('data-price'));
                item.price = (!isNaN(item.price) && item.price > 0) ? item.price : 0;

                item.price_custom = parseInt(_this.attr('data-price_custom'));
                item.price_custom = (!isNaN(item.price_custom) && item.price_custom > 0);

                item.price_min = parseFloat(_this.attr('data-price_min'));
                item.price_min = (!isNaN(item.price_min) && item.price_min > 0) ? item.price_min : 0;

                item.price_max = parseFloat(_this.attr('data-price_max'));
                item.price_max = (!isNaN(item.price_max) && item.price_max > 0) ? item.price_max : 0;

                item.quantity = 1;

                /* Payment */
                formPaymentObj.removeError();

                /* Price && Note*/
                let customPriceObj = formPaymentObj.find('[name="custom_price"]');
                let cus_price = parseFloat(customPriceObj.val());
                cus_price = (!isNaN(cus_price) && cus_price > 0) ? cus_price : 0;
                if (cus_price > 0 && item.price_custom && item.price_min <= cus_price && cus_price <= item.price_max) {
                    item.price = cus_price;
                }
                customPriceObj.val(item.price)
                    .attr({
                        'data-id': item.id,
                        'data-price_custom': item.price_custom ? 1 : 0,
                        'data-price_min': item.price_min,
                        'data-price_max': item.price_max,
                    }).prop('readonly', !item.price_custom);

                let customPriceNoteObj = formPaymentObj.find('#custom_price_note');
                customPriceNoteObj.html(`<p><b>Note:</b> Accept Amount From ${item.price_min} to ${item.price_max}</p>`);
                if (item.price_custom) {
                    customPriceNoteObj.show();
                } else {
                    customPriceNoteObj.hide();
                }

                /* Quantity */
                let customQuantityObj = formPaymentObj.find('[name="custom_quantity"]');
                let cus_quantity = parseInt(customQuantityObj.val());
                cus_quantity = (!isNaN(cus_quantity) && cus_quantity > 0) ? cus_quantity : 0;
                if (cus_quantity > 0) {
                    item.quantity = cus_quantity;
                }
                customQuantityObj.val(item.quantity).attr('data-id', item.id);

                /* Preview */
                _self.changePreviewInfo({
                    'name': item.name,
                    'image': item.image,
                    'amount': 'N/A',
                    'quantity': 'N/A',
                });

                /* Save Cart */
                _self.changeCart(item.id, item.quantity, item.price);
            });

            formPaymentObj.on('keyup change', 'input[name="custom_price"]', function () {
                let _this = $(this);

                /* Deny duplicate click */
                formPaymentObj.find('.btn_payment').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');

                /* Get Item */
                let item = {};
                item.id = parseInt(_this.attr('data-id'));
                item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

                item.price = _this.val();
                item.price = (!isNaN(item.price) && item.price > 0) ? item.price : 0;

                item.price_custom = parseInt(_this.attr('data-price_custom'));
                item.price_custom = (!isNaN(item.price_custom) && item.price_custom > 0);

                item.price_min = parseFloat(_this.attr('data-price_min'));
                item.price_min = (!isNaN(item.price_min) && item.price_min > 0) ? item.price_min : 0;

                item.price_max = parseFloat(_this.attr('data-price_max'));
                item.price_max = (!isNaN(item.price_max) && item.price_max > 0) ? item.price_max : 0;

                /* Save Cart */
                if (item.price_custom && item.price_min <= item.price && item.price <= item.price_max) {
                    formPaymentObj.removeError(['custom_price']);
                    _self.changePrice(item.id, item.price);
                } else {
                    formPaymentObj.addError({
                        'custom_price': `Accept Amount From ${item.price_min} to ${item.price_max}`,
                    });
                }
            });

            formPaymentObj.on('keyup change', 'input[name="custom_quantity"]', function () {
                let _this = $(this);

                /* Deny duplicate click */
                formPaymentObj.find('.btn_payment').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');

                // Get Item
                let item = {};
                item.id = parseInt(_this.attr('data-id'));
                item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

                item.quantity = parseInt(_this.val());

                /* Save Cart */
                if (item.quantity > 0) {
                    formPaymentObj.removeError(['custom_quantity']);
                    _self.changeQuantity(item.id, item.quantity);
                } else {
                    formPaymentObj.addError({'custom_quantity': 'Accept quantity greater than 0'});
                }
            });

            formPaymentObj.on('keyup', 'input[name="ship_full_name"]', function () {
                let _this = $(this);
                _self.changePreviewInfo({
                    'from': _this.val(),
                });
            });

            formPaymentObj.on('keyup', 'input[name="recipient_name"]', function () {
                let _this = $(this);
                _self.changePreviewInfo({
                    'to': _this.val(),
                });
            });
        }
    },

    setDataForm: function (dataForm_JsonString) {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        let boxPaymentItemsObj = $(_self.boxPaymentItems);
        let boxRecipientObj = $(_self.boxRecipient);

        let sendToFriend = formPaymentObj.find('input[name="send_to_friend"]').is(':checked');
        if (sendToFriend) {
            boxRecipientObj.show();
        } else {
            boxRecipientObj.hide();
        }

        if (_self.formPaymentNew) {
            let data = JSON.parse(dataForm_JsonString);

            // Default Price & quantity
            if (data.cus_price > 0) {
                let customPriceObj = formPaymentObj.find('[name="custom_price"]');
                customPriceObj.val(data.cus_price);
            }

            if (data.cus_quantity > 0) {
                let customQuantityObj = formPaymentObj.find('[name="custom_quantity"]');
                customQuantityObj.val(data.cus_quantity);
            }

            // Choose Item
            let paymentItemObj = boxPaymentItemsObj.find(`.paymentItem[data-id="${data.id}"]`);
            if (paymentItemObj.length <= 0) {
                paymentItemObj = boxPaymentItemsObj.find('.paymentItem').first();
            }
            paymentItemObj.trigger('click');
        }
    },

    changeCart: function (id, quantity, cus_price) {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);

        /* Reset Preview*/
        _self.changePreviewInfo({
            'amount': 'N/A',
            'quantity': 'N/A',
        });

        $.ajax({
            type: 'post',
            url: '/cart/changecart',
            data: {id: id, quantity: quantity, cus_price: cus_price},
            dataType: 'Json',
            success: function (obj) {
                obj.id = id;

                let customQuantityObj = formPaymentObj.find('[name="custom_quantity"]');
                customQuantityObj.val(obj.quantity);

                let customPriceObj = formPaymentObj.find('[name="custom_price"]');
                customPriceObj.val(obj.price_new);

                _self.changeCartInfo(obj);
                _self.changePreviewInfo(obj.preview);
                _self.validateNext();
            },
            error: function () {
                call_notify('Notification', 'Error when process request', 'error');
            }
        });
    },

    changePrice: function (id, cus_price) {
        let _self = this;

        /* Reset Preview*/
        _self.changePreviewInfo({
            'amount': 'N/A',
            'quantity': 'N/A',
        });

        $.ajax({
            type: 'post',
            url: '/cart/updateprice',
            data: {id: id, cus_price: cus_price},
            dataType: 'Json',
            success: function (obj) {
                if (obj.status === 'success') {
                    _self.changeCartInfo({
                        'subtotal': obj.cart_data[2],
                        'discount': obj.cart_data[5],
                        'tax': obj.cart_data[1],
                        'amount': obj.cart_data[3],
                        'id': id,
                    });
                    _self.changePreviewInfo(obj.preview);
                    _self.validateNext();
                } else {
                    call_notify('Notification', obj.msg, 'error');
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', 'error');
            }
        });
    },

    changeQuantity: function (id, cus_quantity) {
        let _self = this;

        /* Reset Preview*/
        _self.changePreviewInfo({
            'amount': 'N/A',
            'quantity': 'N/A',
        });

        $.ajax({
            type: 'post',
            url: '/cart/updatequantity',
            data: {id: id, quantity: cus_quantity},
            dataType: 'Json',
            success: function (obj) {
                _self.changeCartInfo({
                    'subtotal': obj.cart_data[2],
                    'discount': obj.cart_data[5],
                    'tax': obj.cart_data[1],
                    'amount': obj.cart_data[3],
                    'id': id,
                });
                _self.changePreviewInfo(obj.preview);
                _self.validateNext();
            },
            error: function () {
                call_notify('Notification', 'Error when process request', 'error');
            }
        });
    },

    changePreviewInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);
        let boxPreviewObj = $(_self.boxPreview);

        if (typeof data.name !== 'undefined') {
            boxCartObj.find('#cart_name').html(data.name);
        }

        if (typeof data.image !== 'undefined') {
            boxCartObj.find('#cart_image img').attr("src", data.image);
            boxPreviewObj.find("#preview_image img").attr("src", data.image);
        }

        if (typeof data.amount !== 'undefined') {
            boxPreviewObj.find('#preview_amount').html(data.amount);
        }

        if (typeof data.quantity !== 'undefined') {
            boxCartObj.find('#cart_quantity').html(data.quantity);
            boxPreviewObj.find("#preview_quantity").html(data.quantity);
        }

        if (typeof data.from !== 'undefined') {
            boxPreviewObj.find("#preview_from").html(data.from);
        }

        if (typeof data.to !== 'undefined') {
            boxPreviewObj.find("#preview_to").html(data.to);
        }

        if (typeof data.message !== 'undefined') {
            boxPreviewObj.find("#preview_message").html(data.message);
        }
    },

    changeCartInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);
        let formPaymentObj = $(_self.formPaymentID);

        boxCartObj.find('#cart_subtotal').html(data.subtotal);
        boxCartObj.find('#cart_discount').html(data.discount);
        boxCartObj.find('#cart_tax').html(data.tax);
        boxCartObj.find('#cart_total').html(data.amount);
        if (data.id) {
            formPaymentObj.find('#custom_price').attr('data-id', data.id);
        }
    },

    validateNext: function () {
        let _self = this;
        let formPaymentObj = $(_self.formPaymentID);
        if (formPaymentObj.find('input.error').length > 0) {
            formPaymentObj.find('.btn_payment').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');
        } else {
            formPaymentObj.find('.btn_payment').prop('disabled', false).removeAttr('disabled').removeClass('disabled');
        }
    }
};

let webCartForm = {
    formCartID: 'form#formCart',
    boxCart: '#boxCart',

    init: function () {
        let _self = this;

        // Events
        _self.setEvents();
    },

    setEvents: function () {
        let _self = this;
        let formCartObj = $(_self.formCartID);
        let boxCartObj = $(_self.boxCart);

        // Validate
        formCartObj.validate({
            submit: {
                settings: {
                    clear: 'keypress',
                    display: "inline",
                    button: "[type='submit']",
                    inputContainer: 'form-group',
                    errorListClass: 'form-tooltip-error',
                },
            }
        });

        // Events
        formCartObj.on('keyup change', 'input.custom_price', function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj.find('.btn_cart').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');

            /* Get Item */
            let item = {};
            item.id = parseInt(_this.attr('data-id'));
            item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

            item.price = _this.val();
            item.price = (!isNaN(item.price) && item.price > 0) ? item.price : 0;

            item.price_custom = parseInt(_this.attr('data-price_custom'));
            item.price_custom = (!isNaN(item.price_custom) && item.price_custom > 0);

            item.price_min = parseFloat(_this.attr('data-price_min'));
            item.price_min = (!isNaN(item.price_min) && item.price_min > 0) ? item.price_min : 0;

            item.price_max = parseFloat(_this.attr('data-price_max'));
            item.price_max = (!isNaN(item.price_max) && item.price_max > 0) ? item.price_max : 0;

            /* Save Cart */
            if (item.price_min <= item.price && item.price <= item.price_max) {
                formCartObj.removeError([`custom_price_${item.id}`]);
                _self.changePrice(item.id, item.price);
            } else {
                let error = {};
                error[`custom_price_${item.id}`] = `Accept Amount From ${item.price_min} to ${item.price_max}`;
                formCartObj.addError(error);
            }
        });

        formCartObj.on('keyup change', 'input.custom_quantity', function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj.find('.btn_cart').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');

            // Get Item
            let item = {};
            item.id = parseInt(_this.attr('data-id'));
            item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

            item.quantity = parseInt(_this.val());

            /* Save Cart */
            if (item.quantity > 0) {
                formCartObj.removeError([`custom_quantity_${item.id}`]);
                _self.changeQuantity(item.id, item.quantity);
            } else {
                let error = {};
                error[`custom_quantity_${item.id}`] = 'Accept quantity greater than 0';
                formCartObj.addError(error);
            }
        });

        formCartObj.on('click', '.delete_cart', function () {
            let _this = $(this);

            /* Deny click to payment*/
            boxCartObj.find('.btn_cart').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');

            // Get Item
            let item = {};
            item.id = parseInt(_this.attr('data-id'));
            item.id = (!isNaN(item.id) && item.id > 0) ? item.id : 0;

            /* Save Cart */
            _self.deleteCart(item.id);
        });
    },

    changePrice: function (id, cus_price) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: 'post',
            url: '/cart/updateprice',
            data: {id: id, cus_price: cus_price},
            dataType: 'Json',
            success: function (obj) {
                if (obj.status === 'success') {
                    formCartObj.find(`#total_change_${id}`).html(obj.total_show);
                    _self.changeCartInfo({
                        'subtotal': obj.cart_data[2],
                        'discount': obj.cart_data[5],
                        'tax': obj.cart_data[1],
                        'amount': obj.cart_data[3],
                        'id': id,
                    });
                    _self.validateNext();
                } else {
                    call_notify('Notification', obj.msg, 'error');
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', 'error');
            }
        });
    },

    changeQuantity: function (id, cus_quantity) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: 'post',
            url: '/cart/updatequantity',
            data: {id: id, quantity: cus_quantity},
            dataType: 'Json',
            success: function (obj) {
                formCartObj.find(`#total_change_${id}`).html(obj.total_show);
                _self.changeCartInfo({
                    'subtotal': obj.cart_data[2],
                    'discount': obj.cart_data[5],
                    'tax': obj.cart_data[1],
                    'amount': obj.cart_data[3],
                    'id': id,
                });
                _self.validateNext();
            },
            error: function () {
                call_notify('Notification', 'Error when process request', 'error');
            }
        });
    },

    deleteCart(id) {
        let _self = this;
        let formCartObj = $(_self.formCartID);

        $.ajax({
            type: 'post',
            url: '/cart/deletecart',
            data: {id: id},
            dataType: 'Json',
            success: function (obj) {
                formCartObj.find(`#cart_item_${id}`).remove();

                let items = formCartObj.find('.cart_item');
                if (items.length <= 0) {
                    formCartObj.find('#cart_items').html('<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>');
                }

                _self.changeCartInfo({
                    'subtotal': obj.cart_data[2],
                    'discount': obj.cart_data[5],
                    'tax': obj.cart_data[1],
                    'amount': obj.cart_data[3],
                    'id': id,
                });
                _self.validateNext();
            }
        });
    },

    changeCartInfo: function (data) {
        let _self = this;
        let boxCartObj = $(_self.boxCart);

        boxCartObj.find('#cart_subtotal').html(data.subtotal);
        boxCartObj.find('#cart_discount').html(data.discount);
        boxCartObj.find('#cart_tax').html(data.tax);
        boxCartObj.find('#cart_total').html(data.amount);
    },

    validateNext: function () {
        let _self = this;
        let formCartObj = $(_self.formCartID);
        let boxCartObj = $(_self.boxCart);
        if (formCartObj.find('input.error').length > 0) {
            boxCartObj.find('.btn_cart').prop('disabled', true).attr('disabled', 'disabled').addClass('disabled');
        } else {
            boxCartObj.find('.btn_cart').prop('disabled', false).removeAttr('disabled').removeClass('disabled');
        }
    }
};

(function ($) {
    'use strict';
    /* REMOVE VALIDATE MSG */
    $('body').on('select2:close, change, focus', 'select, input', function (e) {
        clearValidateMsg($(this));
    });
})(jQuery);

function initImageMagnificPopup(elementClass) {
    let groups = {};
    $(elementClass).each(function () {
        let id = $(this).attr('data-group');
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function () {
        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: {enabled: true}
        });
    });
}

function showValidateMsg( objThis, msg ) {
    if( !msg ){
        msg = objThis.attr('data-validation-message');
    }
    objThis.addClass('error');
    objThis.parent().append(`<div class="form-tooltip-error"><ul><li>${msg}</li></ul></div>`);
}
function clearValidateMsg( objThis) {
    objThis.removeClass("error");
    objThis.parent().find('.form-tooltip-error').remove();
}
function clearAllValidateMsg(objForm) {
    objForm.find('.error').removeClass('error');
    objForm.find('.form-tooltip-error').remove();
}

const stackBottomRightModal = {
    dir1: "up",
    dir2: "left",
    firstpos1: 25,
    firstpos2: 25,
    push: "bottom",
};
var call_notify_object = {};
function callNotify(title_msg, msg, type_notify, delay, remove, type ) {
    type_notify = type_notify ? type_notify : "error";
    delay = delay ? +delay : 3000;
    remove = (typeof remove == 'undefined' || remove) ?  true : false;

    let icon = "";
    if(type_notify == "error" || type_notify == "notice") {
        icon = "fa fa-exclamation-circle";
    } else if(type_notify == "success") {
        icon = "fa fa-check-circle";
    }

    if( remove && typeof call_notify_object.remove === 'function' )
    {
        call_notify_object.remove();
    }

    let option = {
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,

        closer: true,
        closerHover: true,
        sticker: false,
        stickerHover: false,
        labels: {close: 'Close', stick: 'Stick', unstick: 'Unstick'},
        classes: {closer: 'closer', pinUp: 'pinUp', pinDown: 'pinDown'},

        remove: true,
        destroy: true,
        mouseReset: true,
        delay: delay,
    }

    if( !type ){
        option.addclass = 'alert-with-icon stack-bottomright';
        option.stack = stackBottomRightModal;
    } else {
        option.addclass = 'alert-with-icon';
    }

    call_notify_object = new PNotify(option);

    return call_notify_object;
}

function call_notify(title_msg, msg, type_notify) {
    callNotify(title_msg, msg, type_notify, 0, 1, 1 );
}

function change_content(elemenThis, elemenTo) {
    $(elemenTo).html($(elemenThis).val());
}

function check_enter_number(evt, onthis) {
    if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
        return false;
    }
}

(function ($) {
    'use strict';
})(jQuery);

$(document).ready(function(){
    /*MAGIC POPUP*/
    initImageMagnificPopup('.m-magnific-popup');
    initImageMagnificPopup('.image-magnific-popup');

});

/********OLD*********/
(function ($) {
    'use strict';

    /**
    * Set date: Init date time picker for booking
    * Note: place here for deny error when load booking email form in first
    */
    Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
    }
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if(beforeTime == undefined || beforeTime == '' || beforeTime<0){
        beforeTime = 0;
    }
    var fourHoursLater =new Date().addHours(beforeTime);

    var set_date = parseInt(beforeDay) > 0 ? new Date(future.setDate(today.getDate()+beforeDay)) :  fourHoursLater;
        set_date = moment(set_date).format(dateFormatBooking);
        set_date = moment(set_date, dateFormatBooking).toDate();

    $('#datetimepicker_v1, .booking_date').datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });
    // End set date
    
    /*-------------------------------------------
     02. wow js active
     --------------------------------------------- */
    new WOW().init();

    $(document).ready(function(){
        $("body").on("click",".btn-call", function(){
            $(this).prop("disabled", true);
            var obj = $(this);
            $.ajax({
                type: "post",
                url: "/home/count_click",
                success: function(response)
                {
                    // console.log(response);
                    $(obj).prop("disabled", false);
                }
            })
        });
    });



    /*-------------------------------------------
     03. Sticky Header
     --------------------------------------------- */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $("#sticky-header-with-topbar").removeClass("scroll-header");
        } else {
            $("#sticky-header-with-topbar").addClass("scroll-header");
        }
    });




    /*--------------------------------
     /*-------------------------------------------
     05. Portfolio  Masonry (width)
     --------------------------------------------- */
    $(window).load(function () {
       
        $('.list-gallery').magnificPopup({
            delegate: 'a.fancybox',
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: {
                enabled: true
            },
            
        });

        // START SLIDER HOME
        if ( $('.tp-banner li').length > 0 ) {
            function setEqualSlideHeight(selector) {

                $(selector).show();

                var heights = [];
                var widths = [];

                $(selector).each(function() {
                    heights.push($(this).find('img').height());
                    widths.push($(this).find('img').width());
                });

                var maxheights = 660;
                if ( heights.length > 0 ) { 
                    maxheights = Math.max.apply( Math, heights );
                }

                var maxwidths = 1170;
                if ( widths.length > 0 ) {
                    maxwidths = Math.max.apply( Math, widths );
                }

                $('.tp-banner').revolution({
                    delay: 10000,
                    hideThumbs: 10,
                    startwidth: maxwidths,
                    startheight: maxheights,
                });

                $(selector).hide();
            }

            // set Height slide
            setEqualSlideHeight('.tp-banner-img');

            // When resize then reload social
            $(window).on('resize', function(){
                // Firing resize event only when resizing is finished
                clearTimeout(window.resizedFinishedSlider);
                window.resizedFinishedSlider = setTimeout(function(){
                    $('.tp-rightarrow').trigger('click');
                }, 250);
            });
        }
        // END SLIDER HOME
        
        $(".video-play, .bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").addClass("show-overlay");
            var getSrc = $(".overlay").attr('src');
            $(".overlay").find(".show-iframe").html('<iframe src="" frameborder="0" allowfullscreen></iframe>');
            $(".show-iframe iframe").attr("src", getSrc);
        });
        $(".bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").removeClass("show-overlay");
            $(".show-iframe iframe").attr("src", "");
        });
        
        $('.arrow-footer').click(function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });
        $('.item-gallery').each(function () {
            $(this).hover(function () {
                $(this).toggleClass("active");
            });
        });

        $('.main-content').on("mouseover",".item-gallery",function () {
            $(".item-gallery").removeClass("active");
                $(this).addClass("active");
            });


        
        // /* ======= shuffle js ======= */
        // if ($('#portfolio-grid').length > 0) {
        //     /* initialize shuffle plugin */
        //     var $grid = $('#portfolio-grid');

        //     $grid.shuffle({
        //         itemSelector: '.portfolio-item' // the selector for the items in the grid
        //     });

        //     /* reshuffle when user clicks a filter item */
        //     $('#filter li').on('click', function (e) {
        //         e.preventDefault();

        //         // set active class
        //         $('#filter li').removeClass('active');
        //         $(this).addClass('active');

        //         // get group name from clicked item
        //         var groupName = $(this).attr('data-group');

        //         // reshuffle grid
        //         $grid.shuffle('shuffle', groupName);
        //     });
        // }

    });
    /*-------------------------------------------
     06. UI Tab
     --------------------------------------------- */
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
    $('[data-toggle="tooltip"]').tooltip();

    /*-------------------------------------------
     07. button add services
     --------------------------------------------- */
    /*$(document).ready(function () {
        // The maximum number of options
        var MAX_OPTIONS = 5;

       $.fn.is_on_screen = function(){
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' )
        {
            return false;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();
        //
        if ( viewport.top >=  bounds.top) {
            var t =true;
           $('.btn_service_book').addClass('scroll_btn');
           $('.btn_service_book').css('right',viewport.right-bounds.right);
        }else{
            var t= false;
            
        }
        return t;
    };
       $(window).scroll(function(){ 
           if($('.btn_service_defale').is_on_screen()){
              
           }else{
               $('.btn_service_book').removeClass('scroll_btn');
               $('.btn_service_book').removeAttr('style');
           }
             
    
     });

        
    });*/

    /*-------------------------------------------
     8. Modal login form
     --------------------------------------------- */
    $(".databooktime").on("click", ".popup_login", function () {
        $.magnificPopup.open({
            type: 'inline',
            midClick: true,
            items: {
                src: '#popup_login'
            },
        });
        return false;
    })


    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_booking",
                inputContainer: '.input-box',
                errorListClass: 'form-tooltip-error',

            }
        }
    });

    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            }
        }
    });
        

    // SERVICE PAGE
    $("ul.listcatser li").mouseover(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
        $(this).addClass("ui-state-active");
    });

    $("ul.listcatser li").mouseout(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
    });

    // Auto select
    $("select.auto_select").each(function () {
        var val_default = $(this).attr("defaultvalue");
        $(this).find("option[value='" + val_default + "']").prop("selected", true);
    });

    var lid = $('input[name="lid"]').val();
    lid = parseInt(lid); lid = isNaN(lid) ? 0 : lid;
    if ( lid >= 0 ) {
        if ( $('ul.listcatser li[lid="'+lid+'"] a').length <= 0 ) {
            lid = $("ul.listcatser li:first").attr('lid');
            lid = parseInt(lid); lid = isNaN(lid) ? 0 : lid;
        }
        loadService(lid);
    }

    // END SERVICE PAGE

    // BOOKING PAGE
    $(document).on("change", "#surveyForm .list_service", function () {
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");
        
        if (service_id)
        {

            $(this).parent().find('.form-tooltip-error').remove();
        } else
        {

            $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>' + $(this).data('validation-message') + '</li></ul></div>');
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for (var x in obj)
        {
            option += '<option value="' + obj[x].id + '" urlimg="' + obj[x].image + '">' + obj[x].name + '</option>';
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        // Save form
        saveForm();

    });
    // END BOOKING PAGE

    // BTN SEARCH BOOKING
    $(document).on('click',".btn_action",function () {
        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;
        $(".list_service").each(function () {
            var checkval = $(this).val();
            if (checkval)
            {
                $(this).css("border-color", "#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            } else
            {
                check = false;
                $(this).css("border-color", "red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>' + $(this).data('validation-message') + '</li></ul></div>');
            }
            temp.price = $('option:selected', this).attr('price');
            temp.service = $('option:selected', this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });

        var j = 0;
        $(".list_staff").each(function () {
            var checkval = $(this).val();
            temp.image = $('option:selected', this).attr('urlimg');
            temp.name = checkval ? $('option:selected', this).text() : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if (check == true)
        {
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for (var x in info_staff)
            {
                var image = typeof (info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;

                html_person += '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 info-staff">';
                html_person += '<div class="row">';
                html_person += '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 img-info-staff">';
                html_person += '<a href="javascript:;">';
                html_person += '<img src="' + image + '" alt="' + info_staff2[x].name + '">';
                html_person += '</a>';
                html_person += '</div>';
                html_person += '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 title-staff">';
                html_person += '<h2>' + info_staff2[x].name + '</h2>';
                html_person += '<p>' + info_staff[x].service + '</p>';
                html_person += '<p>Price: ' + info_staff[x].price + '</p>';
                html_person += '</div>';
                html_person += '</div>';
                html_person += '</div>';
            }

            $("#box_person").html(html_person);

            var typehtml = $("#surveyForm .choose_date").attr("typehtml");
            var date_choose = $("#surveyForm .choose_date").val();
            pushHtmlTime(date_choose, typehtml);

            var scroll = $("#box_person").offset().top;
            $('body').animate({scrollTop: scroll}, 600, 'swing');//.scrollTop( $("#book-info").offset().top );
            $('.time-booking.databooktime').show();
        } else
        {
            return false;
        }

    });
    // END BTN SEARCH BOOKING

    // CHOOSE DATE
    $("#surveyForm").on("dp.change", ".choose_date", function () {

        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // set Html date
        setHtmldate(date_choose);
        // Save form
        saveForm();

        // change time by date choose
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // $(".choose_date").trigger("dp.change");

    // CHOOSE DATE
    $("#send_booking").on("dp.change", ".choose_date", function () {

        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        // change time by date choose
        pushHtmlTime(date_choose, typehtml);
        //data time
        // setTimeout(function(){ pushHtmlTime(date_choose, typehtml); }, 100);
    });
    // END CHOOSE DATE

    // Booking provider
    $("#surveyForm").on("change", ".list_staff", function () {
        // Save form
        saveForm();
    });
    // End booking provider

    // CONFIRM BOOKING
    $(document).ready(function () {
        $("body").on("click", ".open_booking", function () {
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                    src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function () {
                        if ($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function () {
            $.magnificPopup.close();

        });
    });
    // END CONFIRM BOOKING

    // Mask Input
    var plholder = phoneFormat == "(000) 000-0000" ? "Phone (___) ___-____" : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, {placeholder: plholder});
    // End mask input

    /*Gallery*/
    $("#filter_select").change(function(){
        var id = $(this).val();

        // Set active
        $('#filter li').removeClass('active');
        $('#filter li a[itemprop="'+id+'"]').parent('li').addClass("active");

        // Call Ajax
        getGalleryByCat(id, 1, '#gallery_content');
    });

    $("#filter li a").click(function(e){
        e.preventDefault();

        var id = $(this).attr("itemprop");
        $("#filter_select option[value='"+id+"']").prop("selected", true).trigger('change');
    });

    $("#filter_select").prop("selectedIndex", 0).trigger("change");

    // $("select[name='filter_select']").trigger("change");
    // check form
    $(document).ready(function(){
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function(token)
            {
                $("form").each(function(){
                    $(this).prepend("<input type='hidden' name='token' value='"+token+"' />");
                });
            }
        });
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });
})(jQuery);

function getGalleryByCat(cat_id, page, elementContent) {
    cat_id = cat_id ? cat_id : 0;
    page = page ? page : 0;

    // Category Status
    let objOptionGalleryByCat = $('#optionGalleryByCat_' + cat_id);
    if ( objOptionGalleryByCat.length <= 0 ) {
        objOptionGalleryByCat = $('#optionGalleryByCat');
    }

    let categoryStatus = objOptionGalleryByCat.attr('data-categoryStatus');
    if ( typeof categoryStatus == 'undefined') {
        categoryStatus = 1;
    } else if ( categoryStatus == 'all' ) {
        categoryStatus = false;
    } else {
        categoryStatus = categoryStatus*1;
    }

    let objContent = $(elementContent ? elementContent : '#gallery_content') ;
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');

    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {
            objContent.append(mask_loading_obj);
        },
        data: {cat_id: cat_id, page: page, blockId: elementContent, cat_status: categoryStatus},
        success: function (response) {
            let obj = JSON.parse(response);

            let html = `<div class="m-gallery-box-wrap">`;

            if ( obj.data.length > 0 ) {
                html += `<div class="row">`;
                for (var x in obj.data) {
                    html += `
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-6 check-height">
                        <div class="pointer m-magnific-popup" data-group="gallery-${cat_id}" 
                             title="${obj.data[x].name}" href="${obj.data[x].image}">
                            <div class="gallery-item">
                                <span style="background-image: url('${obj.data[x].imageThumb}');">
                                    <img itemprop="image" src="${obj.data[x].imageThumb}" alt="${obj.data[x].image_alt}" style="display: none;">
                                </span>
                            </div>
                        </div>
                    </div>
                    `;
                }
                html += `</div>`;
            } else {
                html = "Not found gallery item in this category.";
            }
            html += `</div>`;

            objContent.find('.listing').html(html);
            objContent.find('.paging').html(obj.paging_ajax);
            initImageMagnificPopup('.m-magnific-popup');
        },
        complete: function () {
            mask_loading_obj.remove();
        }
    });
}


    function loadService(pg_id, _page)
    {
        pg_id = pg_id ? pg_id :0;
        _page = _page ? _page :0;

        var btn_appointment = "";
        if(typeof(enable_booking) != "undefined" && enable_booking==1)
        {
            btn_appointment = "<a class='btn btn-primary btn_make_appointment' href='/book'>Make an appointment</a>";
        }

        $("ul.listcatser li").removeClass("ui-tabs-active ui-state-active");
        $("ul.listcatser li[lid='"+pg_id+"']").addClass("ui-tabs-active ui-state-active");

        $("select.category_tabs").val(pg_id);

        $.ajax({
            type: "post",
            url: "/service/loadservice",
            data: {pg_id: pg_id, limit: num_paging, page: _page, paging: 1},
            beforeSend: function() {
                $(".content_service").html("Loading...");
            },
            success: function(html)
            {
                var obj = JSON.parse(html);
                $(".paging_service").html(obj.paging_ajax);
                var group_des = obj.group_des;
                obj = obj.data;
                if(obj.length > 0)
                {
                    var html_row = '<ul id="all-item" class="services_item_ul_v1">';

                    /*html_row += '<li class="item-botton services_item_v1 clearfix text-right">';
                    html_row += btn_appointment;
                    html_row += '<a class="btn btn-primary" style="margin-left:15px;" href="tel:' + company_phone + '"><span class="fa"><i class="fa fa-phone"></i></span><span class="title">Call now</span></a>';
                    html_row += '</li>';*/

                    if(group_des)
                    {
                        html_row += '<li class="des_service" style="border-top: none; padding: 10px 0;">';
                        html_row += group_des;
                        html_row += '</li>';
                    }

                    var pull_right = "pull-right";
                    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        pull_right = "";
                    }
                    for(x in obj)
                    {
                        var price_show = obj[x].price_sell ? obj[x].price_sell : "";

                        html_row += '<li class="services_item_v1">';
                        html_row += '<div class="line_item_v1">';
                        html_row += '<div class="just_start_line">';
                        html_row += '<div class="box_title">';
                        html_row += '<span class="name_service_v1">'+obj[x].name+'</span>';
                        html_row += '<span class="price_service_v1 '+pull_right+'">'+price_show+obj[x].product_up+'</span>';
                        html_row += '</div>';
                        html_row += '<div class="box_des">'+obj[x].product_description+'</div>';
                        html_row += '</div>';
                        html_row += '</div>';
                        html_row += '</li>';
                    }

                    html_row += '</ul>';

                    $(".content_service").html(html_row);

                    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
                    if( $(".box_service").length > 0 ){
                        $('body, html').animate({
                            scrollTop: $(".box_service").offset().top-100
                        }, 1000);
                    }
                }else
                {
                    $(".content_service").html("No services found in this category");
                }
            }
        });
        // Load gallery right
        loadGallery(pg_id);
    }

    function loadGallery(pg_id)
    {
        if( !pg_id ){
            pg_id = 0;
        }

        if(pg_id)
        {
            $.ajax({
                type: "post",
                url: "/service/loadgallery",
                data: {id:pg_id},
                beforeSend: function()
                {
                    // $(".box_show_gallery").html("Loading...");
                },
                success: function(html)
                {
                    // console.log(html);
                    var obj = JSON.parse(html);
                    var html_img = '';
                    for(var x in obj)
                    {
                        html_img +='<li>';
                        html_img +='<img itemprop="image" alt="" src="'+obj[x].image+'" class="img-responsive">';
                        html_img +='</li>';
                    }

                    $(".box_show_gallery").html(html_img);
                }
            });
        }
    }

function saveForm()
{
    // Save form
    var formdata = $("#surveyForm").serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formdata,
        success: function (html)
        {
            // console.log(html);
        }
    });
}

function loadForm(formdata)
{
    var obj = JSON.parse(formdata);
    $("input[name='booking_date']").val(obj.booking_date);
    $("input[name='booking_hours']").val(obj.booking_hours);
    var listservice = typeof (obj.service_staff) != "undefined" ? obj.service_staff : [];
    // console.log(listservice);
    if (listservice.length > 0)
    {
        for (var x in listservice)
        {
            // split info
            var list = listservice[x].split(',');
            // Trigger add row
            if (x > 0)
            {
                $(".addButton").trigger("click");
            }
            var objservice = $(".list_service:last");
            $(".list_service:last option[value='" + list[0] + "']").attr("selected", "selected");
            objservice.trigger("change");
            $(".list_staff:last option[value='" + list[1] + "']").attr("selected", "selected");

        }

        // Trigger action
        $(".btn_action").trigger("click");
    }
}

function convertDate(input)
{
    var list_date = input.split("/");
    var splitDate = posFormat.split(",");
    var new_date = list_date[splitDate[2]] + "/" + list_date[splitDate[1]] + "/" + list_date[splitDate[0]];
    return new_date;
}

function pushHtmlTime(input_date, type)
{
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: {input_date: input_date, type: type},
        beforeSend: function(){
            $(".box_detail_info").append("<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>");
            $(".box_detail_info").css("position","relative");
            $(".mask_booking").css("position","absolute").css("height","100%").css("width","100%").css("top",0).css("left",0).css("background","rgba(0,0,0,0.5)").css("text-align","right");
            $(".mask_booking i").css("font-size","2em").css("margin","10px");
        },
        success: function(response)
        {
            // console.log(response);
            // Remove mask
            $(".mask_booking").remove();
            var obj = JSON.parse(response);
            if(obj.checkmorning == false)
            {
                $(".note_am_time").html("(Booking time has expired)");
            }else
            {
                $(".note_am_time").html("");
            }

            if(obj.checkafternoon == false)
            {
                $(".note_pm_time").html("(Booking time has expired)");
            }else
            {
                $(".note_pm_time").html("");
            }

            $(".databooktime .timemorning").html(obj.htmlMorning);
            $(".databooktime .timeafternoon").html(obj.htmlAfternoon);
        }
    });
}

function setHtmldate(date_choose)
{
    // use for booking
    var new_date = convertDate(date_choose);
    var d = new Date(new_date);
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var str_show = days[d.getDay()] + ", " + months[d.getMonth()] + "-" + d.getDate() + "-" + d.getFullYear();
    // console.log(str_show);
    $(".time_show").html(str_show);
}

function loadEvent()
{
    $('#surveyForm')

            // Add button click handler
            .on('click', '.addButton', function () {
                var html_close = '<div class="removeButton"><img src="/public/library/global/remove-service-icon-new.png"></div>';
                var template = '<div class="item-booking">' + html_close + $('#optionTemplate').html() + '</div>';
                $(this).before($(template));
                $("#surveyForm .item-booking:last .list_service").trigger('change');
                saveForm();
            })

            // Remove button click handler
            .on('click', '.removeButton', function () {
                var $row = $(this).parents('.item-booking'),
                        $option = $row.find('[name="option[]"]');

                // Remove element containing the option
                $row.remove();
                saveForm();
            })
}


    function changeTimeByDate(input_date, typehtml)
    {
        // check date time
        var splitDate = posFormat.split(",");//1,0,2
        // change time
        $.ajax({
            type:"post",
            url: "/book/change_time",
            data: {date: input_date},
            success: function(response)
            {
                // console.log(response);
                if(response)
                {
                    var obj = JSON.parse(response);
                    timeMorning = JSON.stringify(obj.time_morning);
                    // convert time afternoon
                    var afternoon_time = obj.time_afternoon;
                    for(var x in afternoon_time)
                    {
                        var listTime = afternoon_time[x].split(":");

                        if(listTime[0] >=1 && listTime[0] < 12)
                        {
                            var changeTime = parseInt(listTime[0])+12;
                            afternoon_time[x] = changeTime+":"+listTime[1];
                        }
                    }
                    
                    timeAfternoon = JSON.stringify(afternoon_time);
                    pushHtmlTime(input_date, typehtml);
                }
            }
        });

    }

function scrollJumpto ( jumpto, headerfixed, redirect )
{
    // check exits element for jumpto
    if ( $(jumpto).length > 0 )
    {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).height()+35 : 35;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    // Check redirect if not exits element for jumpto
    else if ( redirect )
    {
        // Call redirect
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' Not found.');
    }
}

function isOnScroll ( container, selector, header, boundSubtraction ) {
    container = ( typeof container == "undefined" ) ?  "" : container;
    selector = ( typeof selector == "undefined" ) ?  "" : selector;
    header = ( typeof header == "undefined" ) ? "" : header;

    /*Check exit element*/
    if ( ! $(container).length || ! $(selector).length ) {return false;}

    /*Append element instead*/
    let injectSpace = $('<div />', { height: $(selector).outerHeight(true), class: 'injectSpace' + new Date().getTime() }).insertAfter($(selector));
    injectSpace.hide();

    /*Check scroll*/
    $(window).scroll(function(){
        if ( $(container).is_on_scroll( selector, header, boundSubtraction ) ) {
            injectSpace.show();
        }else{
            injectSpace.hide();
        }
    });
}

function isFreezeHeader ( wrapFreezeHeader , flagFreezeHeader, device) {
    let deviceName = device == 'mobile' ? 'mobile' : 'desktop';
    let wrapFreezeHeaderObj = $(wrapFreezeHeader);
    let flagFreezeHeaderObj = $(flagFreezeHeader);

    if( !flagFreezeHeaderObj.hasClass('initializedFreezeHeader') && wrapFreezeHeaderObj.length > 0 && flagFreezeHeaderObj.length > 0 ){
        flagFreezeHeaderObj.addClass('initializedFreezeHeader');
        wrapFreezeHeaderObj.addClass('fixed-freeze '+deviceName);

        let insteadFreezeHeaderObj = $('<div class="instead-flag-freeze-header '+deviceName+'"></div>');
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);

        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass('freeze-header '+deviceName+' with-bg');
                insteadFreezeHeaderObj.height('0px');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass('freeze-header '+deviceName+' with-bg');
            }
        });
    }
}

$(document).ready(function () {
    /*IS ON SCROLL*/
    $.fn.is_on_scroll = function(selector, header, boundSubtraction ) {
        /*Calculate viewport*/
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /*Calculate bounds*/
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}

        if ( $(boundSubtraction).length > 0 ) {
            let boundSubtractionHeight = $(boundSubtraction).outerHeight(true);
            let boundMaxWidth = $(boundSubtraction).attr('bound-maxwidth');
            boundMaxWidth = (typeof boundMaxWidth == "undefined") ? 0 : boundMaxWidth;
            if ( boundMaxWidth > 0 && window.matchMedia('(max-width: ' + boundMaxWidth + 'px)').matches != true ) {
                boundSubtractionHeight = 0;
            }
            bounds.top = bounds.top + boundSubtractionHeight;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        /*Calculate header fixed*/
        let headerHeight = 0;
        if ( $(header).length > 0 ) {
            headerHeight = $(header).outerHeight(true);

            /*Check fixed*/
            let checkFixed = $(header).attr('checkfixed');
            checkFixed = (typeof checkFixed == "undefined") ? 'false' : checkFixed;
            if ( checkFixed === "true" && $(header).css('position') != 'fixed' ){
                headerHeight = 0;
            }

            /*Check max width*/
            let maxWidth = $(header).attr('header-maxwidth');
            maxWidth = ( typeof maxWidth == undefined ) ? 0 : maxWidth;
            if ( maxWidth > 0 && window.matchMedia('(max-width: ' + maxWidth + 'px)').matches != true ) {
                headerHeight = 0;
            }
        }

        if ( viewport.top >= ( bounds.top - headerHeight ) && viewport.top <= ( bounds.bottom - headerHeight ) ) {
            $(selector).css({
                'position': 'fixed',
                'top': headerHeight + 'px',
                'right': (viewport.right - bounds.right) + 'px',
                'z-index': '1001',
            });
            return true;
        } else {
            $(selector).css({
                'position': '',
            });
            return false;
        }
    }

    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    /*FREEZE HEADER*/
    let activeFreezeHeader = $('[name="activeFreezeHeader"]').val();
    if( activeFreezeHeader == 1 || activeFreezeHeader == 3 ){
        isFreezeHeader ( '.wrap-freeze-header' , '.flag-freeze-header');
    }

    if( activeFreezeHeader == 1 || activeFreezeHeader == 2 ){
        isFreezeHeader ( '.wrap-freeze-header-mobile' , '.flag-freeze-header-mobile', 'mobile');
    }

    /*SCROLL BUTTON SERVICE*/
    if (window.matchMedia('(min-width: 992px)').matches){
        isOnScroll ( '.service-container' , '.btn_service_book', '.fixed-freeze.desktop');
    } else {
        isOnScroll ( '.service-container' , '.btn_service_book', '.fixed-freeze.mobile');
    }

    /*
    * TESTIMONIALS
    * */
    $(".testimonials-list").owlCarousel({
        loop: true,
        nav: true,
        margin: 30,
        dots: false,
        items: 1,
        smartSpeed: 1000,
        navText: ['<i class="fa fa-caret-left"></i>','<i class="fa fa-caret-right"></i>'],
    });

    $(window).load(function() {
        if ( $('.animation_sroll_jumpto .sroll_jumpto').length > 0 ) {
            if (window.matchMedia('(min-width: 992px)').matches){
                scrollJumpto('#sci_' + $('input[name="group_id"]').val(), '.fixed-freeze.desktop');
            } else {
                scrollJumpto('#sci_' + $('input[name="group_id"]').val(), '.fixed-freeze.mobile');
            }
        }
    });
});

$(document).ready(function(){
    $('.faq-collapse.show-one').each(function () {
        let _this = $(this);
        _this.on('show.bs.collapse','.collapse', function() {
            _this.find('.collapse.in').collapse('hide');
        });
    });
});
function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});