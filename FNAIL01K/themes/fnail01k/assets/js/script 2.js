$(document).ready(function() {
	/*////////////// MY SLIDER ///////////////*/
    $( '#my-slider' ).sliderPro({
        width: 1976,
        height:800,
        arrows: true,
        fade: true,
        autoHeight:true,
        centerImage:false,
        autoScaleLayers: false,
        buttons: true,
        thumbnailArrows: true,
        autoplay: true,
        slideSpeed : 300,
        breakpoints: {
            768: {
                arrows: true,
                fade: true,
                autoHeight:true,
                centerImage:false,
                autoScaleLayers: false,
                forceSize: 'fullWidth',
                buttons: false,
                thumbnailArrows: false,
                autoplay: true,
                slideSpeed : 300,
            }
        },

    });

     $( '#sale-slider').sliderPro({
        width:"100%",
        height:535,
        arrows: true,
        buttons: true,  
        autoplay:true,
         autoHeight:true,
        autoplayDelay:3000,
        orientation:"vertical",
        breakpoints: {
            990: {width: '100%', autoHeight:true,},
            480: {width: '100%', autoHeight:true,}
          
        }
    });
	/*////////////// MOBILE NAV ///////////////*/
    let menuMobileOptionObj = $('#menuMobileOption');
    let menuMobileOption = {
        meanScreenWidth: parseInt(menuMobileOptionObj.attr('data-meanScreenWidth')),
        meanRevealPosition: menuMobileOptionObj.attr('data-meanRevealPosition'),
        meanMenuOpen: menuMobileOptionObj.attr('data-meanMenuOpen'),
    }
	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: menuMobileOption.meanScreenWidth ? menuMobileOption.meanScreenWidth : 991,
        meanRevealPosition: menuMobileOption.meanRevealPosition ? menuMobileOption.meanRevealPosition : "right",
        meanMenuOpen: menuMobileOption.meanMenuOpen ? menuMobileOption.meanMenuOpen : "<span></span>",
    });
    

    /*////////////// GALLERY ///////////////*/
    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: true,
          gallery: { enabled:true }
      })
      
    });


    var hgroups = {};
    $('.gla-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!hgroups[id]) {
        hgroups[id] = [];
      }       
      hgroups[id].push( this );
    });


    $.each(hgroups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          gallery: { enabled:true }
      })
      
    });


    /*/!*!////////////// BOOKING ///////////////!*!/
    $(document).ready(function() {
      //$('#datetimepicker_v1').datetimepicker();
    });
    // CONFIRM BOOKING
    $(document).ready(function(){
        $(".databooktime").on("click",".open_booking", function(){
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });
    });*/
    

    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

        
})