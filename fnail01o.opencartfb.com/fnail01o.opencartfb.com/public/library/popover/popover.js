/*
* Popover: Extend bootstrap
* */
$(document).ready(function () {
    $('.popup-window').popover({
        html: true,
        title: '<button type="button" class="close" onclick="$(\'.popup-window\').popover(\'hide\');">&times;</button>',
        trigger: 'manual',
        content: function () {
            return $(this).next('.popup-content').html();
        }
    }).click(function (e) {
        $(this).popover('toggle');
        $('.popup-window').not(this).popover('hide');

        e.stopPropagation();
    });

    $('html, body').on('click', function (e) {
        $('.popup-window').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});
/*
* End Popover
* */