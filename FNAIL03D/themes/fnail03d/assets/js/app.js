/* By Theme */
function web_goTo(jumpToElement, redirectURL) {
    scrollJumpto(jumpToElement, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile', redirectURL);
}

/* Booking */
const maskLoadingHtml = '<div class="mask_booking" style="position: absolute; z-index: 2; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>';
let webBookingForm = {
    formID: 'form#formBooking',
    formConfirmID: 'form#formBookingConfirm',
    boxBookingInfo: '#boxBookingInfo',
    boxServiceStaff: '#boxServiceStaff',
    boxDateTime: '#boxDateTime',
    popupBookingConfirm: '#popupBookingConfirm',

    itemIndex: 0,
    categories: {},
    services: {},
    staffs: {},
    getHoursProcess: {
        'queue': '',
        'process': '',
        'sending': false,
    },
    saveFormProcess: {
        'queue': 0,
        'process': 0,
        'sending': false,
    },

    init: function (categories_JsonString, services_JsonString, staffs_JsonString, dataForm_JsonString) {
        let _this = this;

        // Events
        _this.setEvents();

        // Category & Service
        _this.setCategories(categories_JsonString);
        _this.setServices(services_JsonString);

        // Staffs
        _this.setStaffs(staffs_JsonString);

        // Data Form
        _this.setDataForm(dataForm_JsonString);
    },

    setEvents: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let formConfirmObj = $(_this.formConfirmID);

        // Date time picker
        formObj.find('.booking_date').mask(webFormat.dateFormat.replace(/[^\d\/]/g, '0'), {placeholder: webFormat.dateFormat});
        formObj.find('.booking_date').datetimepicker({
            format: webFormat.dateFormat,
            minDate: webBooking.minDate,
            defaultDate: webBooking.minDate,
        });

        formObj.on('dp.change', '.booking_date', function () {
            _this.setOptionCategoryAndServices();
            _this.setOptionStaffs();

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on('change', '.booking_service', function () {
            let itemID = $(this).closest('.booking-item').attr('id');
            _this.setOptionStaff(itemID);

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on('change', '.booking_staff', function () {
            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on('click', '.booking_item_add', function () {
            _this.addItem();
            _this.saveForm();
        });

        formObj.on('click', '.booking_item_remove', function () {
            let itemID = $(this).closest('.booking-item').attr('id');
            _this.removeItem(itemID);

            _this.generalHTML(true);
            _this.saveForm();
        });

        formObj.on('click', '.search_booking', function (e) {
            e.preventDefault();

            let _self = $(this);
            _self.attr('disabled', 'disabled');

            _this.generalHTML();
            _this.saveForm();

            _self.removeAttr('disabled');
        });

        $(`${_this.boxBookingInfo}, ${_this.formID}`).on('click', '.open_booking', function (e) {
            e.preventDefault();

            let _self = $(this);
            formObj.find('[name="booking_hours"]').val(_self.attr('valhours'));

            if (_this.validate()) {
                $.magnificPopup.open({
                    type: 'inline',
                    midClick: true,
                    closeOnBgClick: false,
                    items: {
                        src: _this.popupBookingConfirm,
                    },
                });
            }
        });

        formConfirmObj.on('click', '.btn_cancel', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });

        formConfirmObj.validate({
            submit: {
                settings: {
                    clear: 'keypress',
                    display: "inline",
                    button: ".btn_confirm",
                    inputContainer: 'group-select',
                    errorListClass: 'form-tooltip-error',
                },
                callback: {
                    onSubmit: function (node, formData) {
                        formConfirmObj.find(".btn_confirm").attr("disabled", "disabled");

                        let isValidate = true;
                        formConfirmObj.removeError();

                        if (webGlobal.enableRecaptcha) {
                            let g_recaptcha_response = $("#g-recaptcha-response").val();
                            if (g_recaptcha_response) {
                                formObj.find('[name="g-recaptcha-response"]').val(g_recaptcha_response);
                            } else {
                                isValidate = false;
                                call_notify('Notification', 'Recaptcha is invalid', "error");
                            }
                        }

                        if (isValidate) {
                            formObj.find('[name="booking_name"]').val(formData['booking_name']);
                            formObj.find('[name="booking_phone"]').val(formData['booking_phone']);
                            formObj.find('[name="booking_email"]').val(formData['booking_email']);
                            formObj.find('[name="notelist"]').val(formData['notelist']);
                            formObj.find('[name="store_id"]').val(formData['choose_store']);

                            formObj.submit();
                            return true;
                        } else {
                            formConfirmObj.find(".btn_confirm").removeAttr("disabled");
                            return false;
                        }
                    },
                    onError: function (node, globalError) {
                        let error_msg = '';
                        for (let p in globalError) {
                            error_msg += globalError[p] + '<br>';
                        }
                        call_notify('Notification', error_msg, "error");
                    }
                }
            }
        });
    },

    setCategories: function (categories_JsonString) {
        let _this = this;

        _this.categories = JSON.parse(categories_JsonString);
    },

    setServices: function (services_JsonString) {
        let _this = this;

        _this.services = JSON.parse(services_JsonString);
    },

    setStaffs: function (staffs_JsonString) {
        let _this = this;

        _this.staffs = JSON.parse(staffs_JsonString);
    },

    setDataForm: function (dataForm_JsonString) {
        let _this = this;
        let formObj = $(_this.formID);

        let dataForm_Json = JSON.parse(dataForm_JsonString);

        // Date
        let date = dataForm_Json.booking_date ? dataForm_Json.booking_date : webBooking.minDate;
        formObj.find('.booking_date').val(date);

        // Services & staff
        let generalHtml = false;
        let serviceAndStaffs = dataForm_Json.service_staff ? dataForm_Json.service_staff : [","];
        let ItemCnt = 0;
        for (let x in serviceAndStaffs) {
            ItemCnt++;
            if (ItemCnt > 1) {
                _this.addItem();
            }


            let bookingItemObj = formObj.find('.booking-item').last();
            let bookingItemID = bookingItemObj.attr('id');
            let serviceAndStaff = serviceAndStaffs[x].split(',');

            let serviceID = serviceAndStaff[0];
            _this.setOptionCategoryAndService(bookingItemID, serviceID);

            let staffID = serviceAndStaff[1];
            _this.setOptionStaff(bookingItemID, staffID);

            if (serviceID) {
                generalHtml = true;
            }
        }

        if (generalHtml) {
            _this.generalHTML();
        }
    },

    setOptionCategoryAndServices: function () {
        let _this = this;
        let formObj = $(_this.formID);

        let bookingItemsObj = formObj.find('.booking-item');
        bookingItemsObj.each(function () {
            let bookingItemID = $(this).attr('id');
            _this.setOptionCategoryAndService(bookingItemID);
        });
    },

    setOptionCategoryAndService: function (itemID, serviceID) {
        let _this = this;
        let formObj = $(_this.formID);
        let dateObj = formObj.find('.booking_date');
        let bookingItemObj = formObj.find(`#${itemID}`);
        let serviceObj = bookingItemObj.find('.booking_service');

        let maskLoadingObj = $(maskLoadingHtml);
        serviceObj.parent().append(maskLoadingObj);

        let serviceIDSelected = serviceID ? serviceID : serviceObj.val();
        let dayName = _this.getDayOfWeek(dateObj.val(), true);

        let html = `<option value="">${webForm['booking_service_placeholder']}</option>`;
        for (let x in _this.categories) {
            let category = _this.categories[x];
            let services = _this.categories[x].services;

            let optionServiceHtml = '';
            for (let y in services) {
                let service = _this.getService(services[y], dayName);
                if (service) {
                    let selected = serviceIDSelected * 1 === service.id * 1 ? 'selected' : '';
                    optionServiceHtml += `<option ${selected} value="${service.id}">${service.name}` + (service.price ? ` (${service.price})` : '') + `</option>`;
                }
            }

            if (optionServiceHtml) {
                html += `<optgroup label="${category.name}">${optionServiceHtml}</optgroup>`;
            }
        }
        serviceObj.html(html);
        maskLoadingObj.remove();
    },

    getService: function (serviceID, dayName) {
        let _this = this;

        let service = null;
        if (_this.services && _this.services[serviceID]) {
            if (dayName && _this.services[serviceID].schedule === true) {
                for (let x in _this.services[serviceID].scheduleDay) {
                    if (_this.services[serviceID].scheduleDay[x] === dayName) {
                        service = _this.services[serviceID];
                        break;
                    }
                }
            } else {
                service = _this.services[serviceID];
            }
        }

        return service;
    },

    setOptionStaffs: function () {
        let _this = this;
        let formObj = $(_this.formID);

        let bookingItemsObj = formObj.find('.booking-item');
        bookingItemsObj.each(function () {
            let itemID = $(this).attr('id');
            _this.setOptionStaff(itemID);
        });
    },

    setOptionStaff: function (itemID, staffID) {
        let _this = this;
        let formObj = $(_this.formID);
        let dateObj = formObj.find('.booking_date');
        let bookingItemObj = formObj.find(`#${itemID}`);
        let serviceObj = bookingItemObj.find('.booking_service');
        let staffObj = bookingItemObj.find('.booking_staff');

        let maskLoadingObj = $(maskLoadingHtml);
        staffObj.parent().append(maskLoadingObj);

        let serviceID = serviceObj.val();
        let service = _this.getService(serviceID);
        let staffIDs = service ? service.staffs : null;
        let staffIDSelected = staffID ? staffID : staffObj.val();
        let dayName = _this.getDayOfWeek(dateObj.val(), true);

        let html = `<option value="">${webForm['booking_technician_placeholder']}</option>`;
        for (let x in staffIDs) {
            let staff = _this.getStaff(staffIDs[x], dayName);
            if (staff) {
                let selected = staffIDSelected * 1 === staff.id * 1 ? 'selected' : '';
                html += `<option ${selected} value="${staff.id}">${staff.name}` + (staff.note ? ` (${staff.note})` : '') + `</option>`;
            }
        }
        staffObj.html(html);
        maskLoadingObj.remove();
    },

    getStaff: function (staffID, dayName) {
        let _this = this;

        let staff = null;
        if (_this.staffs && _this.staffs[staffID]) {
            if (dayName && _this.staffs[staffID].schedule === true) {
                for (let x in _this.staffs[staffID].scheduleDay) {
                    if (_this.staffs[staffID].scheduleDay[x] === dayName) {
                        staff = _this.staffs[staffID];
                        break;
                    }
                }
            } else {
                staff = _this.staffs[staffID];
            }
        }

        return staff;
    },

    addItem: function () {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find('.booking-item').last();

        _this.itemIndex++;
        let html = `
            <div class="row booking-service-staff booking-item is-more" id="bookingItem_${_this.itemIndex}">
                <div class="remove-services pointer booking_item_remove"><i class="fa fa-minus-circle"></i></div>
                ` + bookingItemObj.html() + `
            </div>
        `;
        let htmlObj = $(html);
        htmlObj.find('.booking_service').val('');
        htmlObj.find('.booking_staff').val('');

        bookingItemObj.after(htmlObj);
    },

    removeItem: function (itemID) {
        let _this = this;
        let formObj = $(_this.formID);
        let bookingItemObj = formObj.find(`#${itemID}`);

        bookingItemObj.remove();
    },

    validate: function () {
        let _this = this;

        let formObj = $(_this.formID);
        let dateObj = formObj.find('.booking_date');
        let servicesObj = formObj.find('.booking_service');
        let staffsObj = formObj.find('.booking_staff');

        let isValidate = true;
        clearAllValidateMsg(formObj);

        // Date
        if (!dateObj.val()) {
            isValidate = false;
            showValidateMsg(dateObj, webForm ['booking_date_err']);
        }

        // Services
        servicesObj.each(function () {
            let _self = $(this);
            if (!_self.val()) {
                isValidate = false;
                showValidateMsg(_self, webForm ['booking_service_err']);
            }
        });

        // Staffs
        if (webBooking.requiredTechnician) {
            staffsObj.each(function () {
                let _self = $(this);
                if (!_self.val()) {
                    isValidate = false;
                    showValidateMsg(_self, webForm ['booking_technician_err']);
                }
            });
        }

        if (isValidate) {
            return true;
        } else {
            let errorElement = formObj.find('.error').first();
            web_goTo(errorElement.length ? errorElement : formObj);
            return false;
        }
    },

    generalHTML: function (validate) {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        if (validate || _this.validate()) {
            $(_this.boxBookingInfo).show();
            _this.generalHTMLServiceStaff();
            _this.generalHTMLDateTime();
        }
    },

    generalHTMLServiceStaff: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        let formObj = $(_this.formID);
        let servicesObj = formObj.find('.booking_service');
        let staffsObj = formObj.find('.booking_staff');

        let maskLoadingObj = $(maskLoadingHtml);
        $(_this.boxBookingInfo).append(maskLoadingObj);

        let services = [];
        servicesObj.each(function () {
            let _self = $(this).find('option:selected');

            let service = {
                name: 'N/A',
                price: 'N/A',
            };

            let serviceItem = _this.getService(_self.val());
            if (serviceItem) {
                service.name = serviceItem.name;
                service.price = serviceItem.price;
            }

            services.push(service);
        });

        let staffs = [];
        staffsObj.each(function () {
            let _self = $(this).find('option:selected');

            let staff = {
                name: webForm['any_person'],
                image: webGlobal.noPhoto,
                imageIsNo: true,
            };

            let staffItem = _this.getStaff(_self.val());
            if (staffItem) {
                staff.name = staffItem.name;
                staff.image = staffItem.image;
                staff.imageIsNo = staffItem.imageIsNo;
            }

            staffs.push(staff);
        });

        let html = '';
        for (let x in services) {
            html += `
            <div class="service-staff">
                <div class="service-staff-avatar ` + (staffs[x].imageIsNo ? 'no-photo' : '') + `">
                    <img class="img-responsive" src="${staffs[x].image}" alt="${staffs[x].name}">
                </div>
                <div class="service-staff-info">
                    <h5>${staffs[x].name}</h5>
                    <p>${services[x].name}</p>
                    <p>${webForm['price']}: ${services[x].price}</p>
                </div>
            </div>
            `;
        }
        $(_this.boxServiceStaff).html(html);

        maskLoadingObj.remove();
    },

    generalHTMLDateTime: function () {
        let _this = this;

        if (!webBooking.requiredHour) {
            return false;
        }

        _this.getHoursProcess.queue++;
        if (_this.getHoursProcess.sending === false) {
            _this.getHoursProcess.process = _this.getHoursProcess.queue;

            let formObj = $(_this.formID);
            let dateObj = formObj.find('.booking_date');
            let servicesObj = formObj.find('.booking_service');
            let staffsObj = formObj.find('.booking_staff');

            let maskLoadingObj = $(maskLoadingHtml);
            $(_this.boxBookingInfo).append(maskLoadingObj);

            let date = dateObj.val();
            if (date) {
                let serviceIDs = [];
                servicesObj.each(function () {
                    let _self = $(this).find('option:selected');
                    serviceIDs.push(_self.val() * 1);
                });

                let staffIDs = [];
                staffsObj.each(function () {
                    let _self = $(this).find('option:selected');
                    staffIDs.push(_self.val() * 1);
                });

                $.ajax({
                    type: "post",
                    url: "/book/get_hours",
                    data: {input_date: date, input_services: serviceIDs, input_staffs: staffIDs},
                    beforeSend: function () {
                        _this.getHoursProcess.sending = true;
                    },
                    success: function (response) {
                        let responseObj = JSON.parse(response);
                        let boxDateTimeObj = $(_this.boxDateTime);

                        boxDateTimeObj.find('#dateInfo').html(_this.convertDate(responseObj.date));

                        boxDateTimeObj.find('#timeAMHtml').html(responseObj.htmlMorning);
                        boxDateTimeObj.find('#timeAMNote').html(responseObj.checkmorning ? '' : webForm['booking_hours_expired']);

                        boxDateTimeObj.find('#timePMHtml').html(responseObj.htmlAfternoon);
                        boxDateTimeObj.find('#timePMNote').html(responseObj.checkafternoon ? '' : webForm['booking_hours_expired']);
                    },
                    complete: function () {
                        _this.getHoursProcess.sending = false;
                        if (_this.getHoursProcess.queue !== _this.getHoursProcess.process) {
                            _this.generalHTMLDateTime();
                        }

                        maskLoadingObj.remove();
                    }
                });
            } else {
                $(_this.boxDateTime).find('#dateInfo').html('N/A');
                maskLoadingObj.remove();
            }
        }
    },

    saveForm: function () {
        let _this = this;

        _this.saveFormProcess.queue++;
        if (_this.saveFormProcess.sending === false) {
            _this.saveFormProcess.process = _this.saveFormProcess.queue;
            $.ajax({
                type: "post",
                url: "/book/saveform",
                data: $(_this.formID).serialize(),
                beforeSend: function () {
                    _this.saveFormProcess.sending = true;
                },
                complete: function () {
                    _this.saveFormProcess.sending = false;
                    if (_this.saveFormProcess.queue !== _this.saveFormProcess.process) {
                        _this.saveForm();
                    }
                }
            });
        }
    },

    convertDate: function (input) {
        let listDate = input.split('/');
        let splitDate = webFormat.datePosition.split(',');
        let newDate = listDate[splitDate[2]] + '/' + listDate[splitDate[1]] + '/' + listDate[splitDate[0]];
        newDate += '';

        let date = new Date(newDate);
        let months = [webForm['jan'], webForm['feb'], webForm['mar'], webForm['apr'], webForm['may'], webForm['jun'], webForm['jul'], webForm['aug'], webForm['sep'], webForm['oct'], webForm['nov'], webForm['dec']];
        let days = [webForm['sunday'], webForm['monday'], webForm['tuesday'], webForm['wednesday'], webForm['thursday'], webForm['friday'], webForm['saturday']];

        return days[date.getDay()] + ", " + months[date.getMonth()] + "-" + date.getDate() + "-" + date.getFullYear();
    },

    getDayOfWeek: function (input, type) {
        // ISO-8601
        let dayOfWeek_Obj = {
            1: "monday",
            2: "tuesday",
            3: "wednesday",
            4: "thursday",
            5: "friday",
            6: "saturday",
            7: "sunday"
        };

        let dayNumber = moment(input).day();
        dayNumber *= 1;
        if (dayNumber === 0) {
            dayNumber = 7;
        }

        return type ? dayOfWeek_Obj[dayNumber] : dayNumber;
    },
};

const stackBottomRightModal = {
    dir1: "up",
    dir2: "left",
    firstpos1: 25,
    firstpos2: 25,
    push: "bottom",
};
var call_notify_object = {};
function callNotify(title_msg, msg, type_notify, delay, remove, type ) {
    type_notify = type_notify ? type_notify : "error";
    delay = delay ? +delay : 3000;
    remove = (typeof remove == 'undefined' || remove) ?  true : false;

    let icon = "";
    if(type_notify == "error" || type_notify == "notice") {
        icon = "fa fa-exclamation-circle";
    } else if(type_notify == "success") {
        icon = "fa fa-check-circle";
    }

    if( remove && typeof call_notify_object.remove === 'function' )
    {
        call_notify_object.remove();
    }

    let option = {
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,

        closer: true,
        closerHover: true,
        sticker: false,
        stickerHover: false,
        labels: {close: 'Close', stick: 'Stick', unstick: 'Unstick'},
        classes: {closer: 'closer', pinUp: 'pinUp', pinDown: 'pinDown'},

        remove: true,
        destroy: true,
        mouseReset: true,
        delay: delay,
    }

    if( !type ){
        option.addclass = 'alert-with-icon stack-bottomright';
        option.stack = stackBottomRightModal;
    } else {
        option.addclass = 'alert-with-icon';
    }

    call_notify_object = new PNotify(option);

    return call_notify_object;
}

function call_notify(title_msg, msg, type_notify) {
    callNotify(title_msg, msg, type_notify, 0, 1, 1 );
}

function showValidateMsg( objThis, msg ) {
    if( !msg ){
        msg = objThis.attr('data-validation-message');
    }
    objThis.addClass('error');
    objThis.parent().append(`<div class="form-tooltip-error"><ul><li>${msg}</li></ul></div>`);
}
function clearValidateMsg( objThis) {
    objThis.removeClass("error");
    objThis.parent().find('.form-tooltip-error').remove();
}
function clearAllValidateMsg(objForm) {
    objForm.find('.error').removeClass('error');
    objForm.find('.form-tooltip-error').remove();
}


function change_content(elemenThis, elemenTo) {
    $(elemenTo).html($(elemenThis).val());
}

function check_enter_number(evt, onthis) {
    if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
        return false;
    }
}

/* Booking Old */
function convertDate(input) {
    let list_date = input.split("/");
    let splitDate = posFormat.split(",");
    let new_date = list_date[splitDate[2]] + "/" + list_date[splitDate[1]] + "/" + list_date[splitDate[0]];
    return new_date;
}

function setHtmldate(date_choose, locationTo_obj, loading_obj) {
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
    if( loading_obj ){
        loading_obj.append(mask_loading_obj);
    }

    let new_date = convertDate(date_choose);
    let d = new Date(new_date);

    let months = [webForm['jan'], webForm['feb'], webForm['mar'], webForm['apr'], webForm['may'], webForm['jun'], webForm['jul'], webForm['aug'], webForm['sep'], webForm['oct'], webForm['nov'], webForm['dec']];
    let days = [webForm['sunday'], webForm['monday'], webForm['tuesday'], webForm['wednesday'], webForm['thursday'], webForm['friday'], webForm['saturday']];

    let str_show = days[d.getDay()] + ", " + months[d.getMonth()] + "-" + d.getDate() + "-" + d.getFullYear();
    locationTo_obj.html(str_show);

    if( loading_obj ){
        mask_loading_obj.remove();
    }
}

function pushHtmlTime(input_date, type, locationTo_obj, loading_obj) {
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
    $.ajax({
        type: "post",
        url: "/book/get_hours",
        data: {input_date: input_date, type: type},
        beforeSend: function () {
            if( loading_obj ){
                loading_obj.append(mask_loading_obj);
            }
        },
        success: function (response) {
            let obj = JSON.parse(response);

            if( type == 'option' ){
                let html = '<option value="">Select hours</option>';
                html += '<optgroup label="Morning" class="timeMorning">'+obj.htmlMorning+'</optgroup>';
                html += '<optgroup label="Afternoon" class="timeAfternoon">'+obj.htmlAfternoon+'</optgroup>';
                locationTo_obj.html(html);
            } else {
                setHtmldate(input_date, locationTo_obj.find('.time_show'));

                locationTo_obj.find('.note_am_time').html(obj.checkmorning == false ? webForm['booking_hours_expired'] : '');
                locationTo_obj.find('.note_pm_time').html(obj.checkafternoon == false ? webForm['booking_hours_expired'] : '');

                locationTo_obj.find('.html_time_morning').html(obj.htmlMorning);
                locationTo_obj.find('.html_time_afternoon').html(obj.htmlAfternoon);
            }
        },
        complete: function () {
            if( loading_obj ){
                mask_loading_obj.remove();
            }
        }
    });
}

function pushHtmlStaff(input_staff, locationTo_obj, loading_obj) {
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
    if( loading_obj ){
        loading_obj.append(mask_loading_obj);
    }

    let html = '<option value="">'+webForm['booking_technician_placeholder']+'</option>';
    if( input_staff ){
        let obj = JSON.parse(input_staff);
        for ( var x in obj ) {
            html += `<option value="${obj[x].id}" urlimg="${obj[x].image}">${obj[x].name}</option>`;
        }
    }
    locationTo_obj.html(html);

    if( loading_obj ){
        mask_loading_obj.remove();
    }
}

function pushHtmlServiceStaff(input_service_obj, input_staff_obj, locationTo_obj, loading_obj) {
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
    if( loading_obj ){
        loading_obj.append(mask_loading_obj);
    }

    let services = [];
    input_service_obj.each(function () {
        let temp = {};
        temp.price = 'N/A';
        temp.name = 'N/A';
        if( $(this).val() ){
            temp.price = $('option:selected', this).attr('price');
            temp.name = $('option:selected', this).text();
        }

        services.push(temp);
    });

    let staffs = [];
    input_staff_obj.each(function () {
        let temp = {};
        temp.name = webForm['any_person'];
        temp.image = '/public/library/global/no-photo.jpg';
        if( $(this).val() ){
            temp.name = $('option:selected', this).text() ;
            temp.image = $('option:selected', this).attr('urlimg');
        }

        staffs.push(temp);
    });

    let html = '';
    for (var x in services) {
        html += `
        <div class="staff_service_v1 col-sm-6 col-md-6">
            <div class="col-xs-4 staff-avatar">
                <img class="img-responsive" src="${staffs[x].image}" alt="${staffs[x].name}">
            </div>
            <div class="col-xs-8">
                <h4>${staffs[x].name}</h4>
                <p>${services[x].name}</p>
                <p>${webForm['price']}: ${services[x].price}</p>
            </div>
        </div>
        `;
    }
    locationTo_obj.html(html);

    if( loading_obj ){
        mask_loading_obj.remove();
    }
}

function addItemBooking( objForm ) {
    let html = '<div class="row item-booking is-more"><div class="remove-services removeButton pointer"><i class="fa fa-minus-circle"></i></div>'+objForm.find('.item_template').html()+'</div>';
    let last_item_obj = objForm.find('.item-booking').last();
    last_item_obj.after($(html));
}

function saveForm( objForm ) {
    let formData = objForm.serialize();
    $.ajax({
        type: "post",
        url: "/book/saveform",
        data: formData,
        success: function (html) {}
    });
}

function loadForm(formData, formObj, formType) {
    if( formData ){
        let obj = JSON.parse(formData);

        if( formType == 'email' ){
            /*Service, staff*/
            let listService = typeof(obj.service_staff) != "undefined" ? obj.service_staff : [];
            if ( listService.length > 0 ) {
                let booking_service_obj = formObj.find('.booking_service');
                let booking_staff_obj = formObj.find('.booking_staff');
                for (var x in listService) {
                    let service_staff = listService[x].split(',');

                    booking_service_obj.find(`option[value="${service_staff[0]}"]`).attr("selected", "selected");
                    pushHtmlStaff(booking_service_obj.find("option:selected").attr("staff"), booking_staff_obj, booking_staff_obj.parent());

                    booking_staff_obj.find(`option[value="${service_staff[1]}"]`).attr("selected", "selected");
                }
            }

            /*Date, hours*/
            let booking_date_obj = formObj.find('.booking_date');
            let booking_hours_obj = formObj.find('.booking_hours');
            let booking_date = typeof(obj.booking_date) != "undefined" ? obj.booking_date : '';
            if( booking_date != '' ){
                booking_date_obj.val(booking_date);
            }
            pushHtmlTime(booking_date_obj.val(), 'option', booking_hours_obj, booking_hours_obj.parent());

            let booking_hours = typeof(obj.booking_hours) != "undefined" ? obj.booking_hours : '';
            if( booking_hours != '' ){
                booking_hours_obj.find(`option[value="${booking_hours}"]`).attr("selected", "selected");
            }
        } else {
            /*Service, staff*/
            let listService = typeof(obj.service_staff) != "undefined" ? obj.service_staff : [];
            if ( listService.length > 0 ) {
                let booking_item_obj = {};
                let booking_service_obj = {};
                let booking_staff_obj = {};
                let service_staff = {};
                for (var x in listService) {
                    if( x > 0 ){
                        addItemBooking(formObj);
                    }

                    service_staff = listService[x].split(',');
                    booking_item_obj = formObj.find('.item-booking:last')
                    booking_service_obj = booking_item_obj.find('.booking_service');
                    booking_staff_obj = booking_item_obj.find('.booking_staff');

                    booking_service_obj.find(`option[value="${service_staff[0]}"]`).attr("selected", "selected");
                    pushHtmlStaff(booking_service_obj.find("option:selected").attr("staff"), booking_staff_obj, booking_staff_obj.parent());

                    booking_staff_obj.find(`option[value="${service_staff[1]}"]`).attr("selected", "selected");
                }
            }
            pushHtmlServiceStaff(formObj.find(".booking_service"), formObj.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));

            /*Date, hours*/
            let booking_date_obj = formObj.find('.booking_date');
            let booking_hours_obj = formObj.find('.booking_hours');
            let booking_date = typeof(obj.booking_date) != "undefined" ? obj.booking_date : '';
            if( booking_date != '' ){
                booking_date_obj.val(booking_date);
            }
            pushHtmlTime(booking_date, 'html', $('#box_date_time'), $('#booking_info'));
            $('#booking_info').show();

            let booking_hours = typeof(obj.booking_hours) != "undefined" ? obj.booking_hours : '';
            if( booking_hours != '' ){
                booking_hours_obj.val(booking_hours);
            }
        }
    }
}

/* Js animation scroll to service id*/
function redirectUrl ( url, target )
{
    // Check target
    if ( typeof target == 'undefined' )
    {
        target = '_self';
    }

    // append element
    var redirect_url = 'redirect_url_' + new Date().getTime();
    $('body').append('<div style="display:none;"><a class="' + redirect_url + '" target="' + target + '">&nbsp;</a></div>');

    // Call event
    var redirect = $('.' + redirect_url);
    redirect.attr('href',url);
    redirect.attr('onclick',"document.location.replace('" + url + "'); return false;");
    redirect.trigger('click');
}
function scrollJumpto ( jumpto, headerfixed, redirect )
{
    // check exits element for jumpto
    if ( $(jumpto).length > 0 )
    {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).height() + 15 : 15;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    // Check redirect if not exits element for jumpto
    else if ( redirect )
    {
        // Call redirect
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' :not found');
    }
}
// End animation scroll to service id

function isOnScroll ( container, selector, header, boundSubtraction ) {
    container = ( typeof container == "undefined" ) ?  "" : container;
    selector = ( typeof selector == "undefined" ) ?  "" : selector;
    header = ( typeof header == "undefined" ) ? "" : header;

    /*Check exit element*/
    if ( ! $(container).length || ! $(selector).length ) {return false;}

    /*Append element instead*/
    let injectSpace = $('<div />', { height: $(selector).outerHeight(true), class: 'injectSpace' + new Date().getTime() }).insertAfter($(selector));
    injectSpace.hide();

    /*Check scroll*/
    $(window).scroll(function(){
        if ( $(container).is_on_scroll( selector, header, boundSubtraction ) ) {
            injectSpace.show();
        }else{
            injectSpace.hide();
        }
    });
}
function isFreezeHeader ( wrapFreezeHeader , flagFreezeHeader, device) {
    let deviceName = device == 'mobile' ? 'mobile' : 'desktop';
    let wrapFreezeHeaderObj = $(wrapFreezeHeader);
    let flagFreezeHeaderObj = $(flagFreezeHeader);

    if( !flagFreezeHeaderObj.hasClass('initializedFreezeHeader') && wrapFreezeHeaderObj.length > 0 && flagFreezeHeaderObj.length > 0 ){
        flagFreezeHeaderObj.addClass('initializedFreezeHeader');
        wrapFreezeHeaderObj.addClass(`fixed-freeze ${deviceName}`);

        let insteadFreezeHeaderObj = $(`<div class="instead-flag-freeze-header ${deviceName}"></div>`);
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);

        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass(`freeze-header ${deviceName} with-bg`);
                insteadFreezeHeaderObj.height('0px');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass(`freeze-header ${deviceName} with-bg`);
            }
        });
    }
}

function initSliderHome( slider, sliderFixedHeight, sliderItem , selector ) {
    if ( $(sliderItem).length <= 0 ) {
        return;
    }

    /*calculator width height slider*/
    $(selector).show();

    /*width-height*/
    let width = '100%';
    let height = $(selector).find('.fixed').height();
    if( height <= 0 ) {
        let heights = [];
        $(selector).each(function() {
            heights.push($(this).find('img').height());
        });

        if ( heights.length > 0 ) {
            height = Math.min.apply( Math, heights );
        }
    };
    height = (height > 0) ? height : 450;

    $(selector).hide();

    /*init slider and remove other slider*/
    let sliderOption = {};
    sliderOption.autoHeight = $(selector).find('.fixed').height() ? false : true;

    let sliderOptionObj = $(selector).find('#slider-option');

    sliderOption.autoplay = sliderOptionObj.attr('data-autoplay');
    sliderOption.autoplay = sliderOption.autoplay == 'false' ? false : true; // true/ false

    sliderOption.autoplayDelay = sliderOptionObj.attr('data-autoplayDelay')*1;
    sliderOption.autoplayDelay = sliderOption.autoplayDelay >= 1000 ? sliderOption.autoplayDelay : 5000; // milliseconds

    sliderOption.autoplayDirection = sliderOptionObj.attr('data-autoplayDirection'); // normal | backwards
    sliderOption.autoplayDirection = ['normal', 'backwards'].indexOf(sliderOption.autoplayDirection) ? sliderOption.autoplayDirection : 'normal'; // normal | backwards

    sliderOption.fade = sliderOptionObj.attr('data-fade');
    sliderOption.fade = sliderOption.fade == 'false' ? false : true; // true/ false

    let sliderInit = sliderOption.autoHeight ? slider : sliderFixedHeight;
    $( sliderInit ).show().sliderPro({
        width: width,
        height: height,
        autoHeight: sliderOption.autoHeight,
        responsive: sliderOption.autoHeight,
        centerImage: false,
        autoScaleLayers: false,

        arrows: true,
        fade: sliderOption.fade,
        buttons: true,
        thumbnailArrows: true,

        autoplay: sliderOption.autoplay,
        autoplayDelay: sliderOption.autoplayDelay,
        autoplayDirection: sliderOption.autoplayDirection,
        slideSpeed : 300,
    });
    if( sliderInit == slider ) {
        $(sliderFixedHeight).remove();
    } else {
        $(slider).remove();
    }
}

function getGalleryByCat(cat_id, page)
{
    cat_id= cat_id ? cat_id: 0;
    page= page ? page: 0;
    // console.log(cat_id);
    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function() {

        },
        data: {cat_id: cat_id, page:page},
        success: function(html) {
            // console.log(html);
            var obj = JSON.parse(html);
            // console.log(obj);
            var html_gallery="";
            if(obj.data.length > 0)
            {
                for(var x in obj.data)
                {
                    html_gallery += `<div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                            <div class="item-gallery">
                                                            <div class="img-item-gallery">
                                                                <div class="img-item">
                                                                <a itemprop="url" rel="group2" class="clearfix gallery-item2" href="`+obj.data[x].image+`">
                                                                    <span style="background-image:url('`+obj.data[x].imageThumb+`')"></span>
                                                                    <img style="display: none" itemprop="image" src="`+obj.data[x].imageThumb+`" alt="`+obj.data[x].name+`">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <a itemprop="url" rel="group1" class="clearfix gallery-item1" href="`+obj.data[x].image+`">
                                                                <div class="social-item-gallery">
                                                                    <div class="vertical">
                                                                        <h2 itemprop="name">
                                                                            <span class="a">
                                                                            `+obj.data[x].name+`
                                                                            </span>
                                                                        </h2>
                                                                        <div class="img-vector-services">
                                                                            <img itemprop="image" src="images/icon-vector-services.png">
                                                                        </div>
                                                                        <span itemprop="name">`+obj.data[x].name+`</span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                        </div>`;

                    ;
                }
            }else
            {
                html_gallery="Not found gallery item in this category.";
            }

            $(".box_list_gallery").html(html_gallery);
            $(".box_paging").html(obj.paging_ajax);
            load_gallery();
        }
    });
}

function load_gallery(){
    $('.gallery-item1').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
    });
    $('.gallery-item2').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
    });
}

function loadService(pg_id, _page)
{
    pg_id= pg_id ? pg_id : 0;
    _page= _page ? _page : 0;
    var btn_appointment = "";
    if(typeof(enable_booking) != "undefined" && enable_booking==1)
    {
        btn_appointment = "<a class='hs-btn btn_2 btn-light mb-15 btn_make_appointment' href='/book'>Make an appointment</a>";
    }
    $(".cate-services ul li.ui-corner-left").removeClass("active");
    $(".cate-services ul li[lid='"+pg_id+"']").addClass("active");
    $.ajax({
        type: "post",
        url: "/service/loadservice",
        data: {pg_id: pg_id, limit: num_paging, page: _page, paging: 1},
        beforeSend: function() {
            $(".content_service").html("Loading...");
        },
        success: function(html)
        {
            var obj = JSON.parse(html);
            $(".paging_service").html(obj.paging_ajax);
            var group_des = obj.group_des;
            obj = obj.data;

            if(obj.length > 0)
            {
                var html_row = '<ul id="all-item">'+
                    '<li class="item-botton services_item_v1 clearfix text-right">'+
                    btn_appointment+
                    '<a class="hs-btn btn_2 btn-light mb-15" style="margin-left:15px;" href="tel:' + company_phone + '"><span class="fa"><i class="fa fa-phone"></i></span><span class="title">Call now</span></a>'+
                    '</li>'
                ;

                if(group_des)
                {
                    html_row += '<li class="des_service" style="border-top: none; padding: 10px 0;">'+
                        group_des+
                        '</li>';
                }

                var pull_right = "pull-right";
                if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                    pull_right = "";
                }
                for(x in obj)
                {
                    var price_show = obj[x].price_sell ? obj[x].price_sell : "";
                    html_row += '<li class="services_item_v1">'+
                        '<div class="line_item_v1">'+
                        '<div class="just_start_line">'+
                        '<a style=\'cursor: pointer;\' class="open_description" data-toggle="tooltip" data-placement="top" title="'+obj[x].description+'">'+
                        '<span>'+obj[x].name+'</span>'+
                        '<span class="price_service_v1 '+pull_right+'">'+price_show+obj[x].product_up+'</span>'+
                        '</a>'+
                        '<div class="box_des">'+
                        obj[x].product_description+
                        '</div>'+
                        '</div>'+
                        '</div>'+
                        '</li>';
                }

                html_row += '</ul>';

                $(".content_service").html(html_row);

                $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
                $('body, html').animate({
                    scrollTop: $(".box_service").offset().top
                }, 1000);
            }else
            {
                $(".content_service").html("No services found in this category");
            }
        }
    });
}

/* GiftCard */
function change_product(pid, quantity) {
    $.ajax({
        type: "post",
        url: "/cart/change_product",
        data: {pid: pid, quantity: quantity},
        dataType: 'Json',
        success: function (obj) {
            obj.pid = pid;
            change_cart_info(obj);
        },
        error: function () {
            call_notify('Notification', 'Error when process request', "error");
        }
    });
}

function update_price(onThis) {
    let _this = $(onThis);
    let id = parseInt(_this.attr("pid"));
    id = isNaN(id) ? 0 : id;
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;
    let min_val = parseFloat(_this.attr("min"));
    min_val = isNaN(min_val) ? 0 : min_val;
    let max_val = parseFloat(_this.attr("max"));
    max_val = isNaN(max_val) ? 0 : max_val;

    /*Update price*/
    if (cus_price >= min_val && cus_price <= max_val) {
        _this.css("border-color", "");
        $('.btn_payment').prop('disabled', false).removeClass('disabled');

        /*Change money*/
        $(".camount").html(cus_price > 0 ? `\$${cus_price}` : 'N/A');

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function (html) {
                let obj = JSON.parse(html);

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                        "pid": id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_payment').prop('disabled', true).addClass('disabled');
    }
}

function update_quantity(onThis) {
    let _this = $(onThis);
    let id = parseInt(_this.attr("pid"));
    id = isNaN(id) ? 0 : id;
    let cus_quantity = parseFloat(_this.val());
    cus_quantity = isNaN(cus_quantity) ? 1 : cus_quantity;

    /*Update quantity*/
    if (cus_quantity > 0) {
        _this.css("border-color", "");
        $('.btn_payment').prop('disabled', false).removeClass('disabled');

        /*Change quantity*/
        $('#cart_quantity').text(cus_quantity);
        $(".cquantity").html(cus_quantity);

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/update",
            data: {quantity: cus_quantity, id: id},
            success: function (html) {
                let obj = JSON.parse(html);

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                        "pid": id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_payment').prop('disabled', true).addClass('disabled');
    }
}

function change_cart_info(data) {
    $("#cart_subtotal").html(data.subtotal);
    $("#cart_product_discount_value, #cart_discount_code_value").html(data.discount);
    $("#cart_tax").html(data.tax);
    $("#cart_payment_total").html(data.amount);
    if (data.pid) {
        $("#custom_price").attr("pid", data.pid);
    }
}

function update_cart(onthis) {
    let _this = $(onthis);
    let id = parseInt(_this.attr("pid"));
    id = isNaN(id) ? 0 : id;
    let cus_quantity = parseFloat(_this.val());
    cus_quantity = isNaN(cus_quantity) ? 1 : cus_quantity;

    /*Update quantity*/
    if (cus_quantity > 0) {
        _this.css("border-color", "");
        $('.btn_cart_order').prop('disabled', false).removeClass('disabled');

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/update",
            data: {quantity: cus_quantity, id: id},
            success: function (html) {
                let obj = JSON.parse(html);

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                /*Change money*/
                if (obj.total_show) {
                    $(`#total_change_${id}`).html(obj.total_show);
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                        "pid": id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_cart_order').prop('disabled', true).addClass('disabled');
    }
}

function update_price_cart(onThis) {
    let _this = $(onThis);
    let id = parseInt(_this.attr("pid"));
    id = isNaN(id) ? 0 : id;
    let cus_price = parseFloat(_this.val());
    cus_price = isNaN(cus_price) ? 0 : cus_price;
    let min_val = parseFloat(_this.attr("min"));
    min_val = isNaN(min_val) ? 0 : min_val;
    let max_val = parseFloat(_this.attr("max"));
    max_val = isNaN(max_val) ? 0 : max_val;

    /*Update price*/
    if (cus_price >= min_val && cus_price <= max_val) {
        _this.css("border-color", "");
        $('.btn_cart_order').prop('disabled', false).removeClass('disabled');

        //Ajax
        $.ajax({
            type: "post",
            url: "/cart/updateprice",
            data: {cus_price: cus_price, id: id},
            success: function (html) {
                let obj = JSON.parse(html);

                if (obj.status == "error") {
                    _this.val(obj.price);
                    call_notify('Notification', obj.msg, "error");
                    return false;
                }

                /*Change money*/
                if (obj.total_show) {
                    $(`#total_change_${id}`).html(obj.total_show);
                }

                if (obj.cart_data) {
                    let data = {
                        "subtotal": obj.cart_data[2],
                        "discount": obj.cart_data[5],
                        "tax": obj.cart_data[1],
                        "amount": obj.cart_data[3],
                        "pid": id,
                    };
                    change_cart_info(data);
                }
            },
            error: function () {
                call_notify('Notification', 'Error when process request', "error");
            }
        });
    } else {
        _this.css("border-color", "red");
        $('.btn_cart_order').prop('disabled', true).addClass('disabled');
    }
}

function delete_cart(onThis) {
    let _this = $(onThis);
    let id = parseInt(_this.attr("pid"));
    id = isNaN(id) ? 0 : id;

    //Ajax
    $.ajax({
        type: "post",
        url: "/cart/delitem",
        data: {id: id},
        success: function (html) {
            let obj = JSON.parse(html);

            /*Delete item*/
            $(`#cart_item_${id}`).remove();

            /*Change order*/
            let cart_items = $(".cart_item");
            if (cart_items.length <= 0) {
                $("#cart_items").html('<tr><td colspan="5"><div class="price-row-col"><b>Cart empty...</b></div></td></tr>');
            }

            if (obj.cart_data) {
                let data = {
                    "subtotal": obj.cart_data[2],
                    "discount": obj.cart_data[5],
                    "tax": obj.cart_data[1],
                    "amount": obj.cart_data[3],
                };
                change_cart_info(data);
            }
        }
    });
}

(function ($) {
    'use strict';

    var url = window.location.pathname; 
    if(url !="/")
    {
        var urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
        // now grab every link from the navigation
        $('.main-menu > ul > li > a').each(function(){
            // and test its normalized href against the url pathname regexp
            if(urlRegExp.test(this.href.replace(/\/$/,''))){
                $(this).parent("li").addClass('active');
            }
        });
        
    }else
    {
        $(".main-menu > ul > li > a[href='/']").parent("li").addClass("active");
    }

    /*Toogle menu mobile*/
    $('.dropdown-toggle > .btn-toggle').click(function () {
        $(this).closest('.dropdown-toggle').find('.dropdown-menu-toggle').first().slideToggle();
    });
    $('.menu-bar-toggle > .btn-toggle').click(function () {
        $(this).closest('.menu-bar-toggle').find('.menu-bar-toggle-container').first().slideToggle();
    });

    /*-------------------------------------------
     02. wow js active
     --------------------------------------------- */
    new WOW().init();
    /*-------------------------------------------
     03. Sticky Header
     --------------------------------------------- */
    $(window).on('scroll', function () {
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $("#sticky-header-with-topbar").removeClass("scroll-header");
        } else {
            $("#sticky-header-with-topbar").addClass("scroll-header");
        }
    });
    /*--------------------------------
     /*-------------------------------------------
     05. Portfolio  Masonry (width)
     --------------------------------------------- */
    $(window).load(function () {
        load_gallery();

        if ( $('.tp-banner li').length > 0 ) {

            function setEqualSlideHeight(selector) {

                $(selector).show();

                var heights = [];
                var widths = [];

                $(selector).each(function() {
                    heights.push($(this).find('img').height());
                    widths.push($(this).find('img').width());
                });

                var maxheights = 500;
                if ( heights.length > 0 ) { 
                    maxheights = Math.max.apply( Math, heights );
                }

                var maxwidths = 1170;
                if ( widths.length > 0 ) {
                    maxwidths = Math.max.apply( Math, widths );
                }

                /*init slider and remove other slider*/
                $('.slider-width-height').show();

                let sliderOption = {};
                let sliderOptionObj = $('.slider-width-height').find('#slider-option');
                sliderOption.delay = sliderOptionObj.attr('data-delay')*1;
                sliderOption.delay = sliderOption.delay >= 1000 ? sliderOption.delay : 10000; // milliseconds

                $('.slider-width-height').hide();

                $('.tp-banner').revolution({
                    delay: sliderOption.delay,
                    hideThumbs: 10,
                    startwidth: maxwidths,
                    startheight: maxheights,
                });

                $(selector).hide();
            }

            // set Height slide
            setEqualSlideHeight('.tp-banner-img');
        }
        $(".video-play, .bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").addClass("show-overlay");
            var getSrc = $(".overlay").attr('src');
            $(".overlay").find(".show-iframe").html('<iframe src="" frameborder="0" allowfullscreen></iframe>');
            $(".show-iframe iframe").attr("src", getSrc);
        });
        $(".bt-menu-trigger, .overlay-btn").click(function () {
            $(".overlay").removeClass("show-overlay");
            $(".show-iframe iframe").attr("src", "");
        });
            $("#owl-our-services").owlCarousel({
                items: 3,
                slideSpeed: 300,
                pagination: false,
                autoPlay: 4000,
                navigation: true,
                navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                lazyLoad: true,
                itemsDesktop: [990, 2],
                itemsDesktopSmall: [600, 1],
                itemsTablet: [560, 1]
            });
            $('.arrow-footer').click(function () {
                $('html, body').animate({scrollTop: 0}, 800);
                return false;
            });
        
        
        $(".owl-testimonials, .owl-blog-news").owlCarousel({
            items: 3,
            slideSpeed: 300,
            pagination: false,
            navigation: true,
            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
            lazyLoad: true,
            itemsDesktop: [990, 2],
            itemsDesktopSmall: [600, 1],
            itemsTablet: [560, 1]
        });
        $('.arrow-footer').click(function () {
            $('html, body').animate({scrollTop: 0}, 800);
            return false;
        });
        $(".check-height").height($(".check-height").width() - 60);
        // $('.item-gallery').each(function () {
            
        // });

        $('.main-content').on("mouseover",".item-gallery",function () {
            $(".item-gallery").removeClass("active");
                $(this).addClass("active");
            });

         $('.coupon_gift').magnificPopup({
             type: 'image',
             gallery: {
                 enabled: true
             },
         });
    });
    /*-------------------------------------------
     06. UI Tab
     --------------------------------------------- */
   
    /*-------------------------------------------
     07. button add services
     --------------------------------------------- */
    $(document).ready(function () {
            $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
            $('[data-toggle="tooltip"]').tooltip();

            $(document).on('click','.registry_ver_1',function (){
                $(this).toggleClass('open');
            });
        // The maximum number of options
        var MAX_OPTIONS = 5;

        var lid = $('input[name="lid"]').val();
        lid = $('.cate-services ul li[lid="'+lid+'"] a');
        if ( lid.length == 0 )
        {
            lid = $(".cate-services ul li:first a");
        }
        lid.trigger("click");
    });

    // SERVICE PAGE
    $("ul.listcatser li").mouseover(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
        $(this).addClass("ui-state-active");
    });

    $("ul.listcatser li").mouseout(function () {
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
    });

    // Auto select
    $("select.auto_select").each(function () {
        var val_default = $(this).attr("defaultvalue");
        $(this).find("option[value='" + val_default + "']").prop("selected", true);
    });
    // END SERVICE PAGE

    // Gallery page
    $('ul#filter li a').click(function () {
        let _this = $(this);
        let id = _this.attr("data-id");

        // Class active
        $('ul#filter li').removeClass('active');
        $(this).parent("li").addClass("active");

        // Class active
        $("select#filter_select").val(id).trigger("change");
    });

    $('select#filter_select').change(function () {
        let _this = $(this);
        let id = _this.val();

        // Call gallery ajax
        getGalleryByCat(id);

        // Class active
        $('ul#filter li').removeClass('active');
        $('ul#filter li a[data-id="'+id+'"]').parent("li").addClass("active");
    });

    $("select#filter_select option:first").trigger("change");
    /* End Gallery page */

    // $("select[name='filter_select']").trigger("change");

    // check form
    $(document).ready(function(){
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function(token)
            {
                $("form").each(function(){
                    $(this).prepend("<input type='hidden' name='token' value='"+token+"' />");
                });
            }
        });
    });

    /*MASK*/
    $(".inputPhone").mask(phoneFormat, {placeholder: phoneFormat == "(000) 000-0000" ? "(___) ___-____" : "____ ___ ____"});

    /*DATE TIME PICKER BOOKING*/
    $('.bookingDate').datetimepicker({
        format: dateFormatBooking,
        minDate: minDateBooking,
        defaultDate: minDateBooking,
    });

    /*EMAIL BOOKING*/
    let formEmailBooking = $("form#emailBooking");

    formEmailBooking.on("change", ".booking_service", function () {
        let booking_staff_obj = formEmailBooking.find('.booking_staff');
        pushHtmlStaff($(this).find("option:selected").attr("staff"), booking_staff_obj, booking_staff_obj.parent());
        saveForm(formEmailBooking);
    });

    formEmailBooking.on("dp.change", ".booking_date", function () {
        let booking_hours_obj = formEmailBooking.find('.booking_hours');
        pushHtmlTime($(this).val(), 'option', booking_hours_obj, booking_hours_obj.parent());
        saveForm(formEmailBooking);
    });

    formEmailBooking.on("change", ".booking_staff, .booking_hours", function () {
        saveForm(formEmailBooking);
    });

    formEmailBooking.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: ".btn_send_appointment",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) {
                    let isValidate = true;

                    /*Deny duplicate click*/
                    formEmailBooking.find(".btn_send_appointment").attr("disabled", "disabled");

                    /*Clears all form errors*/
                    formEmailBooking.removeError();

                    /*Check services multi*/
                    if( formdata['product_id[]'].length <= 0 ){
                        isValidate = false;
                        formEmailBooking.addError({
                            'product_id[]': webForm ['booking_service_err'],
                        });
                    }

                    if ( enableRecaptcha && !$("#g-recaptcha-response").val() ) {
                        isValidate = false;
                    }

                    if( isValidate ){
                        node[0].submit();
                    } else {
                        formEmailBooking.find(".btn_send_appointment").removeAttr("disabled");
                        scrollJumpto(formEmailBooking);
                    }

                    return false;
                },
                onError: function () {
                    scrollJumpto(formEmailBooking);
                }
            }
        }
    });

    /*SMS BOOKING*/
    let formSmsBooking = $("form#smsBooking");

    formSmsBooking.on("click", ".addButton", function () {
        addItemBooking(formSmsBooking);
        pushHtmlServiceStaff(formSmsBooking.find(".booking_service"), formSmsBooking.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));
        saveForm(formSmsBooking);
    });

    formSmsBooking.on("click", ".removeButton", function () {
        let last_item_obj = $(this).closest('.item-booking');
        last_item_obj.remove();
        pushHtmlServiceStaff(formSmsBooking.find(".booking_service"), formSmsBooking.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));
        saveForm(formSmsBooking);
    });

    formSmsBooking.on("change", ".booking_service", function () {
        let booking_staff_obj = $(this).closest('.item-booking').find('.booking_staff');
        pushHtmlStaff($(this).find("option:selected").attr("staff"), booking_staff_obj, booking_staff_obj.parent());
        pushHtmlServiceStaff(formSmsBooking.find(".booking_service"), formSmsBooking.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));
        saveForm(formSmsBooking);
    });

    formSmsBooking.on("change", ".booking_staff", function () {
        pushHtmlServiceStaff(formSmsBooking.find(".booking_service"), formSmsBooking.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));
        saveForm(formSmsBooking);
    });

    formSmsBooking.on("dp.change", ".booking_date", function () {
        pushHtmlTime($(this).val(), 'html', $('#box_date_time'), $('#booking_info'));
        saveForm(formSmsBooking);
    });

    formSmsBooking.on("click", ".btn_action", function (e) {
        e.preventDefault();
        let isValidate = true;

        /*Deny duplicate click*/
        formSmsBooking.find(".btn_action").attr("disabled", "disabled");

        /*Clears all form errors*/
        clearAllValidateMsg(formSmsBooking);

        /*Check services multi*/
        let booking_service_obj = formSmsBooking.find('.booking_service');
        booking_service_obj.each(function () {
            if( $(this).val() ){
                clearValidateMsg($(this));
            }else{
                isValidate = false;
                showValidateMsg($(this), webForm ['booking_service_err']);
            }
        });

        /*Check staff multi*/
        if ( bookingRequiredTechnician == 1 ) {
            let booking_staff_obj = formSmsBooking.find('.booking_staff');
            booking_staff_obj.each(function () {
                if( $(this).val() ){
                    clearValidateMsg($(this));
                }else{
                    isValidate = false;
                    showValidateMsg($(this), webForm ['booking_technician_err']);
                }
            });
        }

        /*Check date*/
        let booking_date_obj = formSmsBooking.find('.booking_date');
        if( booking_date_obj.val() ){
            clearValidateMsg(booking_date_obj);
        }else{
            isValidate = false;
            showValidateMsg(booking_date_obj, webForm ['booking_date_err']);
        }

        if( isValidate ){
            pushHtmlServiceStaff(formSmsBooking.find(".booking_service"), formSmsBooking.find(".booking_staff"), $('#box_service_staff'), $('#booking_info'));
            pushHtmlTime(formSmsBooking.find(".booking_date").val(), 'html', $('#box_date_time'), $('#booking_info'));
            let booking_info_obj = $('#booking_info');
            booking_info_obj.show();
            scrollJumpto(booking_info_obj);
        }

        formSmsBooking.find(".btn_action").removeAttr("disabled");
    });

    $("body").on("click", ".open_booking", function (e) {
        e.preventDefault();
        let isValidate = true;
        formSmsBooking.find('[name="booking_hours"]').val($(this).attr("valhours"));

        /*Check services multi*/
        let services = formSmsBooking.find('.booking_service');
        services.each(function () {
            if( $(this).val() ){
                clearValidateMsg($(this));
            }else{
                isValidate = false;
                showValidateMsg($(this), webForm ['booking_service_err']);
            }
        });

        /*Check staff multi*/
        if ( bookingRequiredTechnician == 1 ) {
            let booking_staff_obj = formSmsBooking.find('.booking_staff');
            booking_staff_obj.each(function () {
                if( $(this).val() ){
                    clearValidateMsg($(this));
                }else{
                    isValidate = false;
                    showValidateMsg($(this), webForm ['booking_technician_err']);
                }
            });
        }

        /*Check date*/
        let booking_date_obj = formSmsBooking.find('.booking_date');
        if( booking_date_obj.val() ){
            clearValidateMsg(booking_date_obj);
        }else{
            isValidate = false;
            showValidateMsg(booking_date_obj, webForm ['booking_date_err']);
        }

        if ( isValidate == true ) {
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                closeOnBgClick: false,
                items: {
                    src: '#open_booking',
                },
            });
        } else {
            scrollJumpto(formSmsBooking);
        }

        return false;
    });

    $("body").on("click", ".btn_cancel", function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    let formSmsBookingConfirm = $("form#smsBookingConfirm");
    formSmsBookingConfirm.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: ".btn_confirm",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) {
                    let isValidate = true;

                    /*Deny duplicate click*/
                    formSmsBookingConfirm.find(".btn_confirm").attr("disabled", "disabled");

                    /*Clears all form errors*/
                    formSmsBookingConfirm.removeError();

                    if ( enableRecaptcha ) {
                        let g_recaptcha_response = $("#g-recaptcha-response").val();
                        if( g_recaptcha_response ){
                            formSmsBooking.find('[name="g-recaptcha-response"]').val(g_recaptcha_response);
                        } else {
                            isValidate = false;
                        }
                    }

                    if( isValidate ){
                        formSmsBooking.find('[name="booking_name"]').val(formdata['booking_name']);
                        formSmsBooking.find('[name="booking_phone"]').val(formdata['booking_phone']);
                        formSmsBooking.find('[name="notelist"]').val(formdata['notelist'] ? formdata['notelist'] : '');
                        formSmsBooking.find('[name="store_id"]').val(formdata['choose_store'] ? formdata['choose_store'] : 0);

                        formSmsBooking.submit();
                    }

                    formSmsBookingConfirm.find(".btn_confirm").removeAttr("disabled");
                    scrollJumpto(formSmsBooking);
                    return false;
                },
                onError: function () {
                    scrollJumpto(formSmsBooking);
                }
            }
        }
    });

    /*GIFTCARDS PAYMENT*/
    let formPaymentGiftcards = $("form#paymentGiftcards");
    formPaymentGiftcards.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: ".btn_payment",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) {
                    let isValidate = true;

                    /*Deny duplicate click*/
                    formPaymentGiftcards.find(".btn_payment").attr("disabled", "disabled");

                    /*Clears all form errors*/
                    formPaymentGiftcards.removeError();

                    /*Check price*/
                    let custom_price = parseFloat(formdata['custom_price']);
                    custom_price = isNaN(custom_price) ? 0 : custom_price;
                    let custom_price_obj = formPaymentGiftcards.find('[name="custom_price"]');
                    let min_val = parseFloat(custom_price_obj.attr("min"));
                    min_val = isNaN(min_val) ? 0 : min_val;
                    let max_val = parseFloat(custom_price_obj.attr("max"));
                    max_val = isNaN(max_val) ? 0 : max_val;

                    if (custom_price < min_val || custom_price > max_val) {
                        isValidate = false;

                        let notify = `Accept Amount From ${min_val} to ${max_val}`;
                        formPaymentGiftcards.addError({
                            'custom_price': notify,
                        });
                    }

                    if (isValidate) {
                        waitingDialog.show("Please wait a moment ...");
                        node[0].submit();
                    } else {
                        formPaymentGiftcards.find(".btn_payment").removeAttr("disabled");
                        scrollJumpto(formPaymentGiftcards);
                    }

                    return false;
                },
                onError: function () {
                    scrollJumpto(formPaymentGiftcards);
                }
            }
        }
    });

    /*FORM PAYMENT*/
    let formPayment = $("form#payment");
    formPayment.validate({
        submit: {
            settings: {
                clear: 'keypress',
                display: "inline",
                button: "[type='submit']",
                inputContainer: 'form-group',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node) {
                    /*Deny duplicate click*/
                    formPayment.find(".btn_payment").attr("disabled", "disabled");

                    waitingDialog.show("Please wait a moment ...");
                    node[0].submit();

                    return false;
                },
                onError: function () {
                    scrollJumpto(formPayment);
                }
            }
        }
    });

    $("body").on("click", ".box_img_giftcard", function (e) {
        e.preventDefault();

        let _this = $(this);

        $(".box_img_giftcard").removeClass("active");
        _this.addClass("active");

        let src_img = _this.find("img").first().attr("src");
        let pid = _this.attr("pid");
        pid = isNaN(pid) ? 0 : pid;
        let name = _this.attr("name");
        let price = parseFloat(_this.attr("price"));
        price = isNaN(price) ? 0 : price;
        let price_custom_enable = parseInt(_this.attr("price_custom_enable"));
        price_custom_enable = isNaN(price_custom_enable) ? 0 : price_custom_enable;
        let price_max_value = parseFloat(_this.attr("price_max_value"));
        price_max_value = isNaN(price_max_value) ? 0 : price_max_value;
        let quantity = 1;

        /*Payer*/
        formPaymentGiftcards.removeError();
        formPaymentGiftcards.find('input').css("border-color", "");

        /*Price*/
        let custom_price_obj = formPaymentGiftcards.find('[name="custom_price"]');
        custom_price_obj.val(price);
        custom_price_obj.attr('pid', pid);
        custom_price_obj.attr('min', price);
        custom_price_obj.attr('max', price_max_value);
        custom_price_obj.prop('readonly', price_custom_enable == 1 ? false : true);

        let custom_price_note_obj = formPaymentGiftcards.find('#custom_price_note');
        if (price_custom_enable == 1) {
            custom_price_note_obj.find('.custom_price_note').text(`From \$${price} to \$${price_max_value}`);
            custom_price_note_obj.show();
        } else {
            custom_price_note_obj.hide();
        }

        /*Quantity*/
        let custom_quantity_obj = formPaymentGiftcards.find('[name="custom_quantity"]');
        custom_quantity_obj.val(quantity).attr('pid', pid);

        /*Cart*/
        $('#cart_image img').attr("src", src_img);
        $('#cart_quantity').text(quantity);
        $('#cart_name').text(name);

        /*Preview*/
        $(".preview_img img").attr("src", src_img);
        $('.camount').html(`\$${price}`);
        $(".cquantity").html(`${quantity}`);

        change_product(pid, quantity);
    });

    $("body").on("click", "input[name='send_to_friend']", function (e) {
        let _this = $(this);
        if (_this.val() == 0) {
            _this.val(1).prop('checked', true);
            $(".box_recipient").show();
        } else {
            _this.val(0).prop('checked', false);
            $(".box_recipient").hide();
        }
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });
})(jQuery);

$(document).ready(function () {
    /*IS ON SCROLL*/
    $.fn.is_on_scroll = function(selector, header, boundSubtraction ) {
        /*Calculate viewport*/
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /*Calculate bounds*/
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}

        if ( $(boundSubtraction).length > 0 ) {
            let boundSubtractionHeight = $(boundSubtraction).outerHeight(true);
            let boundMaxWidth = $(boundSubtraction).attr('bound-maxwidth');
            boundMaxWidth = (typeof boundMaxWidth == "undefined") ? 0 : boundMaxWidth;
            if ( boundMaxWidth > 0 && window.matchMedia('(max-width: ' + boundMaxWidth + 'px)').matches != true ) {
                boundSubtractionHeight = 0;
            }
            bounds.top = bounds.top + boundSubtractionHeight;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        let boundsSelectorObj = $(selector).parent();
        let boundsSelector = boundsSelectorObj.offset();
        boundsSelector.right = boundsSelector.left + boundsSelectorObj.outerWidth();
        boundsSelector.bottom = boundsSelector.top + boundsSelectorObj.outerHeight();

        /*Calculate header fixed*/
        let headerHeight = 0;
        if ( $(header).length > 0 ) {
            headerHeight = $(header).outerHeight(true);

            /*Check fixed*/
            let checkFixed = $(header).attr('checkfixed');
            checkFixed = (typeof checkFixed == "undefined") ? 'false' : checkFixed;
            if ( checkFixed === "true" && $(header).css('position') != 'fixed' ){
                headerHeight = 0;
            }

            /*Check max width*/
            let maxWidth = $(header).attr('header-maxwidth');
            maxWidth = ( typeof maxWidth == undefined ) ? 0 : maxWidth;
            if ( maxWidth > 0 && window.matchMedia('(max-width: ' + maxWidth + 'px)').matches != true ) {
                headerHeight = 0;
            }
        }

        if ( viewport.top >= ( bounds.top - headerHeight ) && viewport.top <= ( bounds.bottom - headerHeight ) ) {
            $(selector).css({
                'position': 'fixed',
                'top': headerHeight + 'px',
                'right': (viewport.right - boundsSelector.right) + 'px',
                'z-index': '1001',
            });
            return true;
        } else {
            $(selector).css({
                'position': '',
            });
            return false;
        }
    }
    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    // Gallery home
    $(".gallery-owl").owlCarousel({
        items: 3,
        slideSpeed: 300,
        autoPlay: 4000,
        navigation: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        pagination: false,
        dots: false,
        lazyLoad: true,
        itemsDesktop: [992, 2],
        itemsDesktopSmall: [768, 1],
        itemsTablet: [576, 1]
    });

    /*SLIDER*/
    initSliderHome('#my-slider', '#my-slider-fixed-height','.sp-slides .sp-slide', '.slider-width-height');

    /*FREEZE HEADER*/
    let activeFreezeHeader = $('[name="activeFreezeHeader"]').val();
    if( activeFreezeHeader == 1 || activeFreezeHeader == 3 ){
        isFreezeHeader ( '.wrap-freeze-header' , '.flag-freeze-header');
    }

    if( activeFreezeHeader == 1 || activeFreezeHeader == 2 ){
        isFreezeHeader ( '.wrap-freeze-header-mobile' , '.flag-freeze-header-mobile', 'mobile');
    }

    /*SCROLL SERVICE*/
    $(window).load(function () {
        if ($('.animation_sroll_jumpto .sroll_jumpto').length > 0) {
            scrollJumpto('#sci_' + $('input[name="group_id"]').val(), window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });

    /*SCROLL BUTTON SERVICE*/
    isOnScroll ( '.service-container' , '.btn_service_book', window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');

});

function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});