$(document).ready(function() {
    "use strict";
    // creat menu sidebar
    $(".menu-bar-lv-1").each(function(){
        $(this).find(".span-lv-1").click(function(){
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-2").toggle(500);
        });
    });
    $(".menu-bar-lv-2").each(function(){
        $(this).find(".span-lv-2").click(function(){
            $(this).toggleClass('rotate-menu');
            $(this).parent().find(".menu-bar-lv-3").toggle(500);
        });
    });
    
    $(".menu-btn-show").click(function() {
        $('.menu-bar-mobile').toggleClass("menu-bar-mobile-show");
        $(".shadow-mobile").fadeIn();
    });
    $(".shadow-mobile").click(function() {
        $('.menu-bar-mobile').removeClass("menu-bar-mobile-show");
        $(this).fadeOut();
    });
    // end
    $('.open-popup-link').magnificPopup({
        type: 'inline',
        midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });
    $("#send_contact_main").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            }
        }
    });
    $("#send_newsletter_home").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: '.item-subscrible',
                errorListClass: 'form-tooltip-error',

            },
              callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $("input[name='newsletter_email']").val();
                    // console.log(url_send);
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: formdata,
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $("input[name='newsletter_email']").val("");

                            // An form
                            if(obj.status == "success")
                            {
                                $("#send_newsletter_home").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                            }
                            return false;

                        }
                        
                    });
                }// End on before submit
            }
        }
    });

});