(function($) {
    'use strict';
    $(window).on('scroll', function() {
        var scroll = $(window).scrollTop();
        if (scroll < 245) {
            $("#sticky-header-with-topbar").removeClass("scroll-header");
        } else {
            $("#sticky-header-with-topbar").addClass("scroll-header");
        }
    });
    $("#tabs li").removeClass("ui-corner-top").addClass("ui-corner-left");
    $('[data-toggle="tooltip"]').tooltip();

    /*Date.prototype.addHours = function(h) {
        this.setTime(this.getTime() + (h*60*60*1000));
        return this;
    }
    var today = new Date(currDateT);
    var future = new Date(currDateT);

    if(beforeTime == undefined || beforeTime == '' || beforeTime<0){
        beforeTime = 0;
    }
    var fourHoursLater =new Date().addHours(beforeTime);

    var set_date = parseInt(beforeDay) > 0 ? new Date(future.setDate(today.getDate()+beforeDay)) :  fourHoursLater;
    set_date = moment(set_date).format(dateFormatBooking);
    set_date = moment(set_date, dateFormatBooking).toDate();

    $('#datetimepicker_v1').datetimepicker({
        format: dateFormatBooking,
        minDate: set_date,
    });*/

    $('#datetimepicker_v1').datetimepicker({
        format: dateFormatBooking,
        minDate: minDateBooking,
        defaultDate: minDateBooking,
    });

    $(".databooktime").on("click",".popup_login", function(){
        $.magnificPopup.open({
            type: 'inline',
            midClick: true,
            items: {
                  src: '#popup_login'
                },
        });
        return false;
    });

    $("#send_contact").validate({
        submit: {
            settings: {
                button: ".btn_contact",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            }
        }
    });

    $("#send_booking").validate({
        submit: {
            settings: {
                button: ".btn_booking",
                inputContainer: '.input-box',
                errorListClass: 'form-tooltip-error',

            }
        }
    });

    $("#send_newsletter").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: '.form-group',
                errorListClass: 'form-tooltip-error',

            },
            callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $("input[name='newsletter_email']").val();
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: {newsletter_email: email},
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $("input[name='newsletter_email']").val("");

                            if(obj.status == "success")
                            {
                                $(".newsletter_v1_inner").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                            }
                        }
                    });
                }
            }
        }
    });

    $("ul.listcatser li").mouseover(function(){
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
        $(this).addClass("ui-state-active");
    });

    $("ul.listcatser li").mouseout(function(){
        $("ul.listcatser li.ui-state-default.ui-corner-left").removeClass("ui-state-active");
        $("ul.listcatser li.ui-tabs-active").addClass("ui-state-active");
    });

    $("select.auto_select").each(function(){
      var val_default = $(this).attr("defaultvalue");
      $(this).find("option[value='"+val_default+"']").prop("selected",true);
    });

    $("ul.listcatser li:first a").trigger("click");

    $("#surveyForm").on("change",".list_service", function(){
        var service_id = $(this).val();
        var list_staff = $(this).find("option:selected").attr("staff");
        if(service_id)
        {
            $(this).css("border-color","#ccc");
            $(this).parent().find('.form-tooltip-error').remove();
        }else
        {
            $(this).css("border-color","red");
            $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
        }
        var obj = JSON.parse(list_staff);
        var option = '<option value="">Service Provider</option>';
        for(var x in obj)
        {
            option += '<option value="'+obj[x].id+'" urlimg="'+obj[x].image+'">'+obj[x].name+'</option>';
        }

        $(this).parents(".item-booking").find(".list_staff").html(option);

        saveForm();

    });

    $(".btn_action").click(function(){
        
        var num = $(".list_service").length;
        var info_staff = [];
        var info_staff2 = [];
        var temp = {};
        var i = 0;
        var check = true;
        $(".list_service").each(function(){
            var checkval = $(this).val();
            if(checkval) 
            { 
                $(this).css("border-color","#ccc");
                $(this).parent().find('.form-tooltip-error').remove();
            }
            else
            {
                check = false;
                $(this).css("border-color","red");
                $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
            }
            temp.price = $('option:selected', this).attr('price');
            temp.service = $('option:selected', this).text();
            info_staff.push(temp);
            temp = {};
            i++;
        });
        
        var j = 0;
        $(".list_staff").each(function(){
            var checkval = $(this).val();
            temp.image = $('option:selected', this).attr('urlimg');
            temp.name = checkval ? $('option:selected', this).text() : "Any person";
            info_staff2.push(temp);
            temp = {};
            j++;
        });

        if(check == true)
        {   
            $(".box_detail_info").show();
            $("#box_person").html("Loading ...");
            var html_person = "";
            var j = 0;
            for(var x in info_staff)
            {
                var image = typeof(info_staff2[x].image) === "undefined" ? "/public/library/global/no-photo.jpg" : info_staff2[x].image;
                html_person += '<div class="staff_service_v1">'
                    +'<div class="info_staff">'
                        +'<a title="staff avatar">'
                            +'<img src="'+image+'" alt="'+info_staff2[x].name+'">'
                        +'</a>'
                    +'</div>'
                    +'<div class="details_staff">'
                        +'<h2>'+info_staff2[x].name+'</h2>'
                        +'<p>'+info_staff[x].service+'</p>'
                        +'<p>Price: '+info_staff[x].price+'</p>'
                    +'</div>'
                +'</div>';
            }

            $("#box_person").html(html_person);


            var typehtml = $('#surveyForm .choose_date').attr("typehtml");
            var date_choose = $('#surveyForm .choose_date').val();
            pushHtmlTime(date_choose, typehtml);


            var scroll = $("#book-info").offset().top;
            $('body').animate({ scrollTop:  scroll}, 600,'swing');
        }else
        {
            return false;
        }
        
    });

    $("#surveyForm").on("dp.change",".choose_date", function(){
        
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        setHtmldate(date_choose);
        saveForm();


        pushHtmlTime(date_choose, typehtml);
    });

    $("#send_booking").on("dp.change",".choose_date", function(){
        
        var typehtml = $(this).attr("typehtml");
        var date_choose = $(this).val();
        pushHtmlTime(date_choose, typehtml);
    });

    $("#surveyForm").on("change",".list_staff", function(){
        saveForm();
    });

    $(document).ready(function(){
        $("body").on("click",".open_booking", function(){
            var check = true;
            $(".list_service").each(function(){
                var checkval = $(this).val();
                if(checkval) 
                { 
                    $(this).css("border-color","#ccc");
                    $(this).parent().find('.form-tooltip-error').remove();
                }
                else
                {
                    check = false;
                    $(this).css("border-color","red");
                    $(this).parent().append('<div class="form-tooltip-error" data-error-list=""><ul><li>'+$(this).data('validation-message')+'</li></ul></div>');
                }
            });
            
            if(check == false)
            {
                return false;
            }
            
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });

        $('.list-gallery').magnificPopup({
            delegate: 'a.fancybox',
            type: 'image',
            gallery: {
                enabled: true
            },
            zoom: {
                enabled: true,
                duration: 300,
                opener: function (element) {
                    return element.closest('.item-gallery').find('.img-item-gallery img');
                }
            }
        });
    });

    var plholder = phoneFormat == "(000) 000-0000" ? "Phone (___) ___-____" : "Phone ____ ___ ____";
    $(".inputPhone").mask(phoneFormat, {placeholder: plholder});

    $(".btn_cart_order").click(function(){
        var obj = $(this);
        $(".list_price").each(function(){
            var check_val = isNaN(parseFloat($(this).val())) ? 0 : parseFloat($(this).val());
            var max_val = parseFloat($(this).attr("max"));
            var min_val = parseFloat($(this).attr("min"));
            if(check_val > max_val || check_val < min_val)
            {
                $(this).css("border-color", "red");
                obj.removeAttr("href");
                return false;
            }
        })
    });

    $(document).ready(function(){
        $.ajax({
            type: "post",
            url: "/security/create",
            success: function(token)
            {
                $("form").each(function(){
                    $(this).prepend("<input type='hidden' name='token' value='"+token+"' />");
                });
            }
        });
    });

    /*Anchor link*/
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze' : '.fixed-freeze-mobile');
        }
    });

})(jQuery);




function scrollJumpto ( jumpto, headerfixed, redirect )
{
    if ( $(jumpto).length > 0 )
    {
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).outerHeight() : 0;
        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    else if ( redirect )
    {
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' Not found.');
    }
}


function call_notify(title_msg, msg, type_notify)
    {
        type_notify = type_notify ? type_notify : "error";

        var icon = "";
        if(type_notify == "error")
        {
            icon = "fa fa-exclamation-circle";
        }else if(type_notify == "success")
        {
            icon = "fa fa-check-circle";
        }
        new PNotify({
            title: title_msg,
            text: msg,
            type: type_notify,
            icon: icon,
            addclass: 'alert-with-icon'
        });
 

    }


    function saveForm()
    {
        var formdata = $("#surveyForm").serialize();
        $.ajax({
            type: "post",
            url: "/book/saveform",
            data: formdata,
            success:function(html)
            {
            }
        });
    }

    function loadForm(formdata)
    {
        var obj = JSON.parse(formdata);
        $("input[name='booking_date']").val(obj.booking_date);
        $("input[name='booking_hours']").val(obj.booking_hours);
        var listservice = typeof(obj.service_staff) != "undefined" ? obj.service_staff : [];
        if(listservice.length > 0 )
        {
            for(var x in listservice)
            {
                var list = listservice[x].split(',');
                if(x>0)
                {
                    $(".addButton").trigger("click");
                }
                var objservice = $(".list_service:last");
                $(".list_service:last option[value='"+list[0]+"']").attr("selected", "selected");
                objservice.trigger("change");
                $(".list_staff:last option[value='"+list[1]+"']").attr("selected", "selected");
                
            }

        }
    }

    function convertDate(input)
    {
        var list_date = input.split("/");
        var splitDate = posFormat.split(",");
        var new_date = list_date[splitDate[2]]+"/"+list_date[splitDate[1]]+"/"+list_date[splitDate[0]];
        return new_date;
    }

    function pushHtmlTime(input_date,type)
    {
        $.ajax({
            type: "post",
            url: "/book/get_hours",
            data: {input_date: input_date, type: type},
            beforeSend: function(){
                $(".box_detail_info").append("<div class='mask_booking'><i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i></div>");
                $(".box_detail_info").css("position","relative");
                $(".mask_booking").css("position","absolute").css("height","100%").css("width","100%").css("top",0).css("left",0).css("background","rgba(0,0,0,0.5)").css("text-align","right");
                $(".mask_booking i").css("font-size","2em").css("margin","10px");
            },
            success: function(response)
            {
                $(".mask_booking").remove();
                var obj = JSON.parse(response);
                if(obj.checkmorning == false)
                {
                    $(".note_am_time").html("(Booking time has expired)");
                }else
                {
                    $(".note_am_time").html("");
                }

                if(obj.checkafternoon == false)
                {
                    $(".note_pm_time").html("(Booking time has expired)");
                }else
                {
                    $(".note_pm_time").html("");
                }
                $(".databooktime").attr('style','display: block');
                $(".databooktime .timemorning").html(obj.htmlMorning);
                $(".databooktime .timeafternoon").html(obj.htmlAfternoon);

            }
        });
    }

    function setHtmldate(date_choose)
    {
        var new_date = convertDate(date_choose);
        var d = new Date(new_date);

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var str_show = days[d.getDay()]+", "+months[d.getMonth()]+"-"+d.getDate()+"-"+d.getFullYear();
        $(".time_show").html(str_show);
    }

    function loadEvent()
    {
        $('#surveyForm')

        .on('click', '.addButton', function() {
            var html_close = '<div class="removeButton"><img src="/public/library/global/remove-service-icon-new.png" alt="remove-service-icon-new.png"></div>';
            var template = '<div class="item-booking">'+html_close+$('#optionTemplate').html()+'</div>';
            $(this).before($(template));
            $("#surveyForm .item-booking:last .list_service").trigger('change');
            saveForm();
        })

        .on('click', '.removeButton', function() {
            var $row = $(this).parents('.item-booking'),
                $option = $row.find('[name="option[]"]');

            $row.remove();
            saveForm();
        })
    }


    function update_cart(onthis)
    {
        var quantity = $(onthis).val();
        var id = $(onthis).attr("cart_id");
        $.ajax({
            type: "post",
            url: "/cart/update",
            data: {quantity: quantity, id: id},
            success: function(html)
            {
                var obj = JSON.parse(html);
                if(obj.total_show && obj.amount)
                {
                    $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                    $(".amount_change").html(obj.amount);
                }
                
            }
        });
    }

    function update_price(onthis)
    {
        var cus_price = isNaN(parseFloat($(onthis).val())) ? 0 : parseFloat($(onthis).val());
        var id = $(onthis).attr("cart_id");
        var max_val = parseFloat($(onthis).attr("max"));
        var min_val = parseFloat($(onthis).attr("min"));
        
        if(cus_price >= min_val && cus_price <= max_val)
        {
            $(onthis).css("border-color", "#ccc");
            $(".btn_cart_order").attr("href","/payment");
            $.ajax({
                type: "post",
                url: "/cart/updateprice",
                data: {cus_price: cus_price, id: id},
                success: function(html)
                {
                    var obj = JSON.parse(html);
                    if(obj.status == "error")
                    {
                        call_notify('Notification',obj.msg, "error");
                        $(onthis).val(obj.price);
                        return false;
                    }
                    if(obj.total_show && obj.amount)
                    {
                        $(onthis).parents("tr").find(".total_change").html(obj.total_show);
                        $(".amount_change").html(obj.amount);
                    }
                    
                }
            });
        }else
        {
            $(onthis).css("border-color", "red");
            $(".btn_cart_order").removeAttr("href");
        }
    }

    function delItem(onthis)
    {
        var id = $(onthis).attr("cart_id");
        $.ajax({
            type: "post",
            url: "/cart/delitem",
            data: {id: id},
            success: function(html)
            {
                var obj = JSON.parse(html);
                if(obj.amount)
                {
                    $(onthis).parents("tr").remove();
                    if($(".list_stt").length > 0)
                    {
                        var i=1;
                        $(".list_stt").each(function(){
                            $(this).html("#"+i);
                            i++;
                        });
                    }else
                    {
                        $("tbody.step1").html('<tr><td colspan="7" style="text-align: center"><b>Cart empty</b></td></tr>');
                    }
                    $(".amount_change").html(obj.amount);
                }
                
            }
        });
    }

    function changeTimeByDate(input_date, typehtml)
    {
        var splitDate = posFormat.split(",");
        $.ajax({
            type:"post",
            url: "/book/change_time",
            data: {date: input_date},
            success: function(response)
            {
                if(response)
                {
                    var obj = JSON.parse(response);
                    timeMorning = JSON.stringify(obj.time_morning);
                    var afternoon_time = obj.time_afternoon;
                    for(var x in afternoon_time)
                    {
                        var listTime = afternoon_time[x].split(":");

                        if(listTime[0] >=1 && listTime[0] < 12)
                        {
                            var changeTime = parseInt(listTime[0])+12;
                            afternoon_time[x] = changeTime+":"+listTime[1];
                        }
                    }
                    
                    timeAfternoon = JSON.stringify(afternoon_time);
                    pushHtmlTime(input_date, typehtml);
                }
            }
        });

    }

$(document).ready(function (){
    /*
    * Freeze
    * */
    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    /* Check scroll */
    var wrapFreezeHeaderObj = $('.wrap-freeze-header');
    var flagFreezeHeaderObj = $('.flag-freeze-header');
    var btnServiceBook = $('.btn_service_book');
    if( wrapFreezeHeaderObj.find('[name="activeFreezeHeader"]').val() ){
        flagFreezeHeaderObj.addClass('fixed-freeze');
        var insteadFreezeHeaderObj = $('<div class="instead-flag-freeze-header"></div>');
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);
        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass('freeze-header freeze-header-desktop with-bg');
                insteadFreezeHeaderObj.height('0px');
                btnServiceBook.removeClass('freeze-btn');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass('freeze-header freeze-header-desktop with-bg');
                btnServiceBook.addClass('freeze-btn');
            }
        });
    }

    /* Check scroll */
    var wrapFreezeHeaderObj_mobile = $('.wrap-freeze-header');
    var flagFreezeHeaderObj_mobile = $('.flag-freeze-header-mobile');
    var btnServiceBook_mobile = $('.btn_service_book');
    if( wrapFreezeHeaderObj_mobile.find('[name="activeFreezeHeader"]').val() ){
        flagFreezeHeaderObj_mobile.addClass('fixed-freeze-mobile');
        var insteadFreezeHeaderObj_mobile = $('<div class="instead-flag-freeze-header-mobile"></div>');
        insteadFreezeHeaderObj_mobile.insertBefore(flagFreezeHeaderObj_mobile);
        $(window).scroll(function(){
            if( wrapFreezeHeaderObj_mobile.is_on_scroll1() ){
                flagFreezeHeaderObj_mobile.removeClass('freeze-header freeze-header-mobile with-bg');
                insteadFreezeHeaderObj_mobile.height('0px');
                btnServiceBook_mobile.removeClass('freeze-btn-mobile');
            } else {
                insteadFreezeHeaderObj_mobile.height(flagFreezeHeaderObj_mobile.outerHeight()+'px');
                flagFreezeHeaderObj_mobile.addClass('freeze-header freeze-header-mobile with-bg');
                btnServiceBook_mobile.addClass('freeze-btn-mobile');
            }
        });
    }

    if ( $('.animation_sroll_jumpto .sroll_jumpto').length > 0 ) {
        scrollJumpto('#sci_' + $('input[name="group_id"]').val(), window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze' : '.fixed-freeze-mobile');
    }
});

function initSliderHome( slider, sliderFixedHeight, sliderItem , selector ) {
    if ( $(sliderItem).length <= 0 ) {
        return;
    }

    /*calculator width height slider*/
    $(selector).show();

    /*width-height*/
    let width = '100%';
    let height = $(selector).find('.fixed').height();
    if( height <= 0 ) {
        let heights = [];
        $(selector).each(function() {
            heights.push($(this).find('img').height());
        });

        if ( heights.length > 0 ) {
            height = Math.min.apply( Math, heights );
        }
    };
    height = (height > 0) ? height : 450;

    $(selector).hide();

    /*init slider and remove other slider*/
    let sliderOption = {};
    sliderOption.autoHeight = $(selector).find('.fixed').height() ? false : true;

    let sliderOptionObj = $(selector).find('#slider-option');
    sliderOption.autoplay = sliderOptionObj.attr('data-autoplay');
    sliderOption.autoplay = sliderOption.autoplay == 'false' ? false : true; // true/ false

    sliderOption.autoplayDelay = sliderOptionObj.attr('data-autoplayDelay')*1;
    sliderOption.autoplayDelay = sliderOption.autoplayDelay >= 1000 ? sliderOption.autoplayDelay : 5000; // milliseconds

    let sliderInit = sliderOption.autoHeight ? slider : sliderFixedHeight;
    $( sliderInit ).show().sliderPro({
        width: width,
        height: height,
        autoHeight: sliderOption.autoHeight,
        responsive: sliderOption.autoHeight,
        centerImage: false,
        autoScaleLayers: false,

        arrows: true,
        fade: true,
        buttons: true,
        thumbnailArrows: true,

        autoplay: sliderOption.autoplay,
        autoplayDelay : sliderOption.autoplayDelay,
    });
    if( sliderInit == slider ) {
        $(sliderFixedHeight).remove();
    } else {
        $(slider).remove();
    }
}

function initImageMagnificPopup(elementClass) {
    let groups = {};
    $(elementClass).each(function () {
        let id = $(this).attr('data-group');
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function () {
        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: {enabled: true}
        });
    });
}

/* Gallery */
function initGalleryTab( elementTab, elementContent ) {
    let objTab = $(elementTab);

    objTab.on("click", ".tab", function () {
        let _this = $(this);
        let id = _this.attr("data-id");

        // Class active
        objTab.find('.tab').removeClass('active');
        _this.addClass("active");

        getGalleryByCat(id, 1, elementContent);
    });

    // First load
    objTab.find('.tab').first().trigger('click');
}
function getGalleryByCat(cat_id, page, elementContent) {
    cat_id = cat_id ? cat_id : 0;
    page = page ? page : 0;

    /*Category Status*/
    let objOptionGalleryByCat = $('#optionGalleryByCat_' + cat_id);
    if ( objOptionGalleryByCat.length <= 0 ) {
        objOptionGalleryByCat = $('#optionGalleryByCat');
    }

    let categoryStatus = objOptionGalleryByCat.attr('data-categoryStatus');
    if ( typeof categoryStatus == 'undefined') {
        categoryStatus = 1;
    } else if ( categoryStatus == 'all' ) {
        categoryStatus = false;
    } else {
        categoryStatus = categoryStatus*1;
    }

    let objContent = $(elementContent);
    let mask_loading_obj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');

    $.ajax({
        type: "post",
        url: "/gallery/getlistbycat",
        beforeSend: function () {
            objContent.append(mask_loading_obj);
        },
        data: {cat_id: cat_id, page: page, blockId: elementContent, cat_status: categoryStatus},
        success: function (response) {
            let obj = JSON.parse(response);

            let html = `<div class="m-gallery-box-wrap">`;

            if ( obj.data.length > 0 ) {
                html += `<div class="row">`;
                for (var x in obj.data) {
                    html += `
                    <div class="col-xs-6 col-sm-6 col-md-4">
                        <div class="pointer m-magnific-popup" data-group="gallery-${cat_id}" 
                             title="${obj.data[x].name}" href="${obj.data[x].image}">
                            <div class="m-gallery-box">
                                <div class="m-image-bg" style="background-image: url('${obj.data[x].imageThumb}');">
                                    <img itemprop="image" src="${obj.data[x].imageThumb}" alt="${obj.data[x].image_alt}">
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                }
                html += `</div>`;
            } else {
                html = "Not found gallery item in this category.";
            }
            html += `</div>`;

            objContent.find('.listing').html(html);
            objContent.find('.paging').html(obj.paging_ajax);
            initImageMagnificPopup('.m-magnific-popup');
        },
        complete: function () {
            mask_loading_obj.remove();
        }
    });
}

$(document).ready(function () {
    /*SLIDER*/
    initSliderHome('#my-slider', '#my-slider-fixed-height','.sp-slides .sp-slide', '.slider-width-height');

    /*GALERY POPUP*/
    initImageMagnificPopup('.m-magnific-popup');

    /*GALLERY TAB*/
    initGalleryTab('#category_tab', '#gallery_content');
});


function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});