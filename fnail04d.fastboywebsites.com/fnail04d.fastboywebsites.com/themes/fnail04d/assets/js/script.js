$(document).ready(function() {
    new WOW().init();


	/*$( '#my-slider' ).sliderPro({
        fade: true,
        width:"100%",
        height:650,
        autoHeight: true,
        responsive: true,
        arrows: true,
        buttons: true,
        autoplay:false,
        autoplayDelay:3000,
        fadeDuration:3000,
        touchSwipe:false,
        /!*breakpoints: {
            990: {width: '100%',height:450},
            768: {width: '100%',height:350},
            480: {width: '100%',height:200},
            320: {width: '100%',height:150},

        }*!/
    });*/

    $('.animated-arrow').click(function () {
        if($('body').hasClass('navopener')){
            $('body').removeClass('navopener');
            $( ".overblack" ).remove();
        }else{
            $('body').addClass('navopener'); 
            $('body').append('<div class="overblack"></div>');
        }        
    });

  

    var h_doc = $('body').height();
    var h_screen = $(window).height();
    var w_screen = $(window).width();
    var h_main = $('main').height();
    if(w_screen<=768){
        h_screen = h_screen-70;
    }
    $('.section-home').css('height', (h_screen)+'px');

    
    $(function(){
        $(window).scroll(function(){
        
            if($(this).scrollTop()>10){
                $('.nav-left').addClass('stickUp');
            }else{
                $('.nav-left').removeClass('stickUp');
            }
        });
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > h_screen) {
            $('.nav-left').addClass('stickUp');
        }
    });


    if(h_screen>h_doc) {
        var newH = h_screen-h_doc+h_main;
        $('main').css('height', (newH)+'px');
    }


	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: "990",
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>"
    });
    

    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: true,
          gallery: { enabled:true }
      })
      
    });


    var hgroups = {};
    $('.gla-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!hgroups[id]) {
        hgroups[id] = [];
      }       
      hgroups[id].push( this );
    });


    $.each(hgroups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          gallery: { enabled:true }
      })
      
    });


    $(document).ready(function(){
        $(".databooktime").on("click",".open_booking", function(){
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });
    });
    if($(".testimonials-list")) {
        $(".testimonials-list").owlCarousel({
            loop: true,
            nav: true,
            margin: 30,
            dots: false,
            items: 1,
            smartSpeed: 1000,
        });
    }
    if($(".blog-client.owl-carousel")) {
        $(".blog-client.owl-carousel").owlCarousel({
            loop: true,
            nav: false,
            margin: 30,
            dots: false,
            smartSpeed: 1000,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: true,
            responsive: {
                0: {items: 1},
                600: {items: 2},
                1000: {items: 3}
            }

        });
    }

    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

    if($(".owl-service")) {
        $(".owl-service").owlCarousel({
            loop: true,
            nav: true,
            margin: 30,
            dots: false,
            items: 4,
            smartSpeed: 1000,
            responsive: {
                0: {items: 1},
                768: {items: 2},
                991: {items: 3},
                1200: {items: 4}
            }
        });
    }

    if($(".owl-gallery")) {
        $(".owl-gallery").owlCarousel({
            loop: true,
            nav: true,
            margin: 10,
            dots: false,
            items: 4,
            smartSpeed: 1000,
            responsive: {
                0: {items: 1},
                576: {items: 2},
                768: {items: 3},
                992: {items: 4},
                1200: {items: 4}
            }
        });
    }
});