function redirectUrl ( url, target ) 
{
    // Check target
    if ( typeof target == 'undefined' ) 
    {
        target = '_self';
    }

    // append element
    var redirect_url = 'redirect_url_' + new Date().getTime();
    $('body').append('<div style="display:none;"><a class="' + redirect_url + '" target="' + target + '">&nbsp;</a></div>');

    // Call event
    var redirect = $('.' + redirect_url);
        redirect.attr('href',url);
        redirect.attr('onclick',"document.location.replace('" + url + "'); return false;");
        redirect.trigger('click');
}

function scrollJumpto ( jumpto, headerfixed, redirect ) 
{
    // check exits element for jumpto
    if ( $(jumpto).length > 0 ) 
    {
        // Calculator position and call jumpto with effect
        jumpto = $(jumpto).offset().top;
        headerfixed = ( $(headerfixed).length > 0 ) ? $(headerfixed).height() : 15;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - headerfixed) + 'px'
        }, 1000, 'swing');
    }
    // Check redirect if not exits element for jumpto
    else if ( redirect ) 
    {
        // Call redirect
        redirectUrl(redirect);
        return;
    }
    else
    {
        console.log(jumpto + ' Not found.');
    }
}

function isFreezeHeader ( wrapFreezeHeader , flagFreezeHeader, device) {
    let deviceName = device == 'mobile' ? 'mobile' : 'desktop';
    let wrapFreezeHeaderObj = $(wrapFreezeHeader);
    let flagFreezeHeaderObj = $(flagFreezeHeader);

    if( !flagFreezeHeaderObj.hasClass('initializedFreezeHeader') && wrapFreezeHeaderObj.length > 0 && flagFreezeHeaderObj.length > 0 ){
        flagFreezeHeaderObj.addClass('initializedFreezeHeader');
        wrapFreezeHeaderObj.addClass(`fixed-freeze ${deviceName}`);

        let insteadFreezeHeaderObj = $(`<div class="instead-flag-freeze-header ${deviceName}"></div>`);
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);

        $(window).scroll(function(){
            if( wrapFreezeHeaderObj.is_on_scroll1() ){
                flagFreezeHeaderObj.removeClass(`freeze-header with-bg ${deviceName}`);
                insteadFreezeHeaderObj.height('0px');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight()+'px');
                flagFreezeHeaderObj.addClass(`freeze-header with-bg ${deviceName}`);
            }
        });
    }
}

// Init gallery magic popup
function initImageMagnificPopup(){
    var groups = {};
    $('.image-magnific-popup').each(function() {
        var id = $(this).attr('data-group');
        if( ! groups[id] )
        {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function() {
        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: { enabled:true }
        });
    });
}
initImageMagnificPopup();// End init gallery popup

$(document).ready(function(){

    // Jumpto
    function initEventScrollJumto(){
        $('.scroll_jumpto').click(function(event){
            event.preventDefault();

            // input elements
            var jumpto = $(this).data('jumpto');
            var headerfixed = $(this).data('headerfixed');
            var redirect = $(this).data('redirect');

            // call fumpto
            scrollJumpto ( jumpto, headerfixed, redirect );
        });
    }
    initEventScrollJumto();// End Jumpto

    // Init menuMobile
    function initMenuMobile() {
        $('.mobile-menu nav').show().meanmenu({
            meanMenuContainer: '.menu_mobile_v1',
            meanScreenWidth: "991",
            meanRevealPosition: "right",
            meanMenuOpen: "<span></span>"
        });
    }
    initMenuMobile(); // End Init menuMobile    

    // Add Class Active Menu
    function setActiveMenu() {
        if ( typeof site == "undefined" || site == "idx" ) {site = "";}
        if ( typeof site_act == "undefined" ) {site_act = "";}
        if ( site == 'p' ) {site += '/' + site_act;}

        $('.menu_main > li > a[href="/'+site+'"], .menu_main > a[href="/'+site+'"], .menu_mobile > li > a[href="/'+site+'"], .menu_mobile > a[href="/'+site+'"], .footer_menu > li > a[href="/'+site+'"], .footer_menu > a[href="/'+site+'"]').addClass('active').parent().addClass('active');
    }
    setActiveMenu();// End Add Class Active Menu

    // Load slider
    function initSliderHome( slider, sliderFixedHeight, sliderItem , selector ) {
        if ( $(sliderItem).length <= 0 ) {
            return;
        }

        // calculator width height slider
        $(selector).show();

        // width
        var width = $(selector).find('.fixed').width();
            width = (width > 0) ? width : $(selector).find('img').first().width();
            width = (width > 0) ? width : 1024;

        // Height
        var height = $(selector).find('.fixed').height();
        if( height <= 0 ) {
            var heights = [];
            $(selector).each(function() {
                heights.push($(this).find('img').height());
            });

            if ( heights.length > 0 ) { 
                height = Math.min.apply( Math, heights );
            }
        };
        height = (height > 0) ? height : 450; 

        $(selector).hide();
        // end calculator width height slider

        // init slider and remove other slider
        var autoHeight = $(selector).find('.fixed').height() ? false : true;
        var sliderInit = autoHeight ? slider : sliderFixedHeight;
        $( sliderInit ).show().sliderPro({
            width: '100%', 
            height: height, 
            autoHeight: autoHeight,
            responsive: autoHeight, 
            centerImage: false, 
            autoScaleLayers: false, 
            
            arrows: true, 
            fade: true, 
            buttons: true, 
            thumbnailArrows: true, 

            autoplay: true, 
            slideSpeed : 300, 
        });
        if( sliderInit == slider ) {
            $(sliderFixedHeight).remove();
        } else {
            $(slider).remove();
        }
        // end init slider
    }
    initSliderHome('#my-slider', '#my-slider-fixed-height','.sp-slides .sp-slide', '.slider-width-height');// End Load slider

    // Init same height
    // Init same height
    // data-minscreen="992"
    // data-minscreen="992" data-distance="3"
    // data-minscreen='["992", "768"]' data-distance='["3", "2"]'
    function initSameHeight(){
        if ( $('.same_height').length ) 
        {
            $('.same_height').each(function()
            {
                // Inputs
                var sameheight = $(this);
                var minscreen = sameheight.data('minscreen');
                    minscreen = (typeof minscreen == "undefined") ? [] : minscreen;
                    minscreen = Array.isArray(minscreen) == true ? minscreen : [minscreen];

                var distance = sameheight.data('distance');
                    distance = (typeof distance == "undefined") ? [] : distance;
                    distance = Array.isArray(distance) == true ? distance : [distance];

                var items = sameheight.find('.same_height_item');
                    items.css({'min-height': ''}); // Reset height
                    items.attr('data-group', ''); // Reset group
                if ( minscreen.length ) 
                {
                    // used for: browse the array
                    for( x in minscreen )
                    {
                        minscreen[x] *= 1;

                        // group used for set height
                        var group = 'samegroup_' + new Date().getTime() + '_' + minscreen[x];

                        // Check width screen
                        if ( minscreen[x] > 1 && window.matchMedia('(min-width: ' + minscreen[x] + 'px)').matches) 
                        {
                            // General all height for get max height
                            var heights = [];

                            // Check distance and set value
                            distance[x] = (typeof distance[x] == "undefined") ? 1 : distance[x] * 1;

                            // used each: browse the object
                            var i = 0;
                            items.each(function() {
                                i++;

                                // add group
                                var item = $(this);
                                    item.attr('data-group', group);

                                // Check distance
                                if ( distance[x] > 1 && i > 1 && (i-1)%distance[x] == 0 ) 
                                {
                                    // Remove current group
                                    item.attr('data-group', '');

                                    // get max height and set height items
                                    height = Math.max.apply( Math, heights );
                                    height = Math.round(height+1);
                                    sameheight.find('[data-group="'+group+'"]').css({'min-height': height + 'px',});

                                    // create new group and reset heights general and group
                                    group = 'samegroup_' + new Date().getTime() + '_' + minscreen[x];
                                    heights = [];
                                    item.attr('data-group', group);
                                }

                                //Push height to array
                                heights.push(item.height());
                            });

                            // get max height and set
                            height = Math.max.apply( Math, heights );
                            height = Math.round(height+1);
                            sameheight.find('[data-group="'+group+'"]').css({'min-height': height + 'px',});

                            // Important break if already set
                            break;
                        }
                    }
                }
            });
        }
    }
    initSameHeight();
    // End init same height

    /*FREEZE HEADER*/
    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };
    let activeFreezeHeader = $('[name="activeFreezeHeader"]').val();
    if( activeFreezeHeader == 1 || activeFreezeHeader == 3 ){
        isFreezeHeader ( '.wrap-freeze-header' , '.flag-freeze-header');
    }

    if( activeFreezeHeader == 1 || activeFreezeHeader == 2 ){
        isFreezeHeader ( '.wrap-freeze-header-mobile' , '.flag-freeze-header-mobile', 'mobile');
    }

    // Animation scroll to service id
    $(window).load(function() {
        if ( $('.animation_sroll_jumpto .sroll_jumpto').length > 0 ) {
            scrollJumpto('#sci_' + $('input[name="group_id"]').val(), window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });// End animation scroll to service id

    // Scroll Button Service Page
    $.fn.is_on_scroll = function( selector, header, boundSubtraction ) {
        // Not included margin, padding of window
        var win = $(window);
        var viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        // Not included margin of this element: same container
        var bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}

        // Check bound subtraction
        if ( $(boundSubtraction).length > 0 ) {
            var boundSubtractionHeight = $(boundSubtraction).outerHeight(true);// Included margin
            var boundMaxWidth = $(boundSubtraction).attr('bound-maxwidth');
            boundMaxWidth = (typeof boundMaxWidth == "undefined") ? 0 : boundMaxWidth;
            if ( boundMaxWidth > 0 && window.matchMedia('(max-width: ' + boundMaxWidth + 'px)').matches != true ) {
                boundSubtractionHeight = 0;
            }
            bounds.top = bounds.top + boundSubtractionHeight;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        // Calculator header height
        var headerHeight = 0;
        if ( $(header).length > 0 ) {
            // Included margin of header
            var headerHeight = $(header).outerHeight(true);

            // Check fixed
            var checkFixed = $(header).attr('checkfixed');
            checkFixed = (typeof checkFixed == "undefined") ? 'false' : checkFixed;
            if ( checkFixed === "true" && $(header).css('position') != 'fixed' ){
                headerHeight = 0;
            }

            // Check max width
            var maxWidth = $(header).attr('header-maxwidth');
            maxWidth = ( typeof maxWidth == undefined ) ? 0 : maxWidth;
            if ( maxWidth > 0 && window.matchMedia('(max-width: ' + maxWidth + 'px)').matches != true ) {
                headerHeight = 0;
            }
        }

        if ( viewport.top >= ( bounds.top - headerHeight ) && viewport.top <= ( bounds.bottom - headerHeight ) ) {
            // place here for calculator right
            $(selector).css({
                'position': 'fixed',
                'top': headerHeight + 'px',
                'right': (viewport.right - bounds.right) + 'px',
                'z-index': '1001',
            });
            return true;
        } else {
            $(selector).css({
                'position': '',
            });
            return false;
        }
    }
    function isOnScroll ( container, selector, header, boundSubtraction ) {
        container = ( typeof container == "undefined" ) ?  "" : container;
        selector = ( typeof selector == "undefined" ) ?  "" : selector;
        header = ( typeof header == "undefined" ) ? "" : header;

        // Check exit element
        if ( ! $(container).length || ! $(selector).length ) {return false;}

        // Append element instead
        var injectSpace = $('<div />', { height: $(selector).outerHeight(true), class: 'injectSpace' + new Date().getTime() }).insertAfter($(selector));
        injectSpace.hide();

        // Check scroll
        $(window).scroll(function(){
            if ( $(container).is_on_scroll( selector, header, boundSubtraction ) ) {
                injectSpace.show();
            }else{
                injectSpace.hide();
            }
        });
    }
    isOnScroll ( '.content_service_wrap' , '.btn_service', '', '');
    isOnScroll ( '.pc-service-data' , '.sb-service-data', (window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile'), '.hf-service-data');// End Scroll Button Service Page

    // When resize then reload
    $(window).on('resize', function(){
        // Firing resize event only when resizing is finished
        clearTimeout(window.resizedFinishedReload);
        window.resizedFinishedReload = setTimeout(function(){
            // End init same height
            initSameHeight();

            // Scroll menu
            // initScrollMenu ('.header-top-wrap', '.navbar.main-nav');

        }, 250);
    });// When resize then reload
});
function load_social(inputs) {
    if ( !inputs ) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if ( social_block_width < 180 ){
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if ( typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed ) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height)/parseInt(inputs.facebook_embed.width)));
        let  social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header='+(inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs='+inputs.facebook_embed.tabs;
            social_url += '&show_facepile='+(inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces='+(inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream='+(inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += '&appId' + inputs.facebook_embed.appId;

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="'+social_url+'" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

$(document).ready(function () {
    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});