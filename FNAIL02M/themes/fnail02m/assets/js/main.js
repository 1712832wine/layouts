$(document).ready(function() {
    if($('.list-service').length)
    {
        $('.list-service').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows:true,
            responsive: [{
                breakpoint: 1024,
                settings: {slidesToShow: 3}
            },{
                breakpoint: 600,
                settings: {slidesToShow: 2}
            },{
                breakpoint: 320,
                settings: { slidesToShow: 1}
            }]
        });

    }

    if($('.slider-for').length)
    {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.slider-nav'
        });
    }
    if($('.slider-nav').length)
    {
        $('.slider-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            centerPadding: '20px',
            asNavFor: '.slider-for',
            centerMode: true,
            focusOnSelect: true,
        });
    }

     $("#send_newsletter").validate({
        submit: {
            settings: {
                button: ".btn_send_newsletter",
                inputContainer: '.input-newsletter',
                errorListClass: 'form-tooltip-error',
            },
            callback: {
                onSubmit: function (node, formdata) 
                {
                    var url_send = $(node).attr("action");
                    var email = $("input[name='newsletter_email']").val();
                    var token = $("input[name='token']").val();
                    $.ajax({
                        type: "post",
                        url: url_send,
                        data: {newsletter_email: email,token:token},
                        success: function(html)
                        {
                            var obj = JSON.parse(html);
                            call_notify("Notification", obj.message, obj.status);
                            $("input[name='newsletter_email']").val("");

                            // An form
                            if(obj.status == "success")
                            {
                                $("#send_newsletter .input-newsletter").html('<h2 class="newsletter_tile" style="text-align:center">Thanks for subscribing!</h2>');
                            }
                        }
                    });
                }// End on before submit
            }
        }
      
    });
    $("#send_contact_main").validate({
            submit: {
                settings: {
                    button: ".btn_contact",
                    inputContainer: '.form-group',
                    errorListClass: 'form-tooltip-error',

                }
            }
           
        });
        
        //   var minHeight=parseInt($('.list-item-gallery').eq(0).css('height'));
        // $('.list-gallery-nail img.gallery-item-image').each(function () {
        //     var thisHeight = parseInt($(this).css('height'));
        //     if(thisHeight>100){
        //         minHeight=((minHeight<=thisHeight)?minHeight:thisHeight);
        //     }
        // });
        // $('.list-item-gallery').css('height',minHeight+'px');
});