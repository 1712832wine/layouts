(function () {
    "use strict";

    /*Back To Top*/
    $(document).on('click', '.back-to-top', function () {
        $("html,body").animate({
            scrollTop: 0
        }, 2000);
    });
    $(document).on('click', '#back-top', function () {
        $("html,body").animate({
            scrollTop: 0
        }, 2000);
    });
})(jQuery);

$(document).ready(function () {
    /*Back To Top*/
    $(window).on('scroll', function () {
        let ScrollTop = $('.back-to-top');
        if ($(window).scrollTop() > 1000) {
            ScrollTop.fadeIn(1000);
        } else {
            ScrollTop.fadeOut(1000);
        }
    });

    /*Footer Brand*/
    let sponsorsCarousel = $('.sponsors-carousel');
    if (sponsorsCarousel.length > 0) {
        sponsorsCarousel.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
            smartSpeed: 700,
            autoplay: true,
            items: 6,
            responsive: {
                0: {items: 2},
                576: {items: 3},
                768: {items: 5},
                992: {items: 5},
                1200: {items: 6}
            }
        });
    }

    /* Service Slider */
    let serviceCarousel = $('.services-carousel');
    if (serviceCarousel.length > 0) {
        serviceCarousel.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            navText: ['<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>'],
            smartSpeed: 700,
            autoplay: false,
            responsive: {
                0: {items: 1},
                576: {items: 2},
                768: {items: 2},
                992: {items: 3},
                1200: {items: 3}
            }
        });
    }

    /* Countdown */
    let countBoxAppear = $('.count-box');
    if (countBoxAppear.length > 0) {
        countBoxAppear.appear(function () {
            let $t = $(this),
                n = $t.find(".count-text").attr("data-stop"),
                r = parseInt($t.find(".count-text").attr("data-speed"), 10);
            if (!$t.hasClass("counted")) {
                $t.addClass("counted");
                $({
                    countNum: $t.find(".count-text").text()
                }).animate({
                    countNum: n
                }, {
                    duration: r,
                    easing: "linear",
                    step: function () {
                        $t.find(".count-text").text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $t.find(".count-text").text(this.countNum);
                    }
                });
            }
        }, {accY: 0});
    }

    /* Gallery Filters */
    let galleryMixItUp = $('.filter-list');
    if (galleryMixItUp.length > 0) {
        galleryMixItUp.mixItUp({});
    }

    /*Testimonial Slider*/
    let testimonialCarousel = $('.testimonial-carousel');
    if (testimonialCarousel.length > 0) {
        testimonialCarousel.owlCarousel({
            loop: false,
            autoplay: false,
            autoPlayTimeout: 1000,
            margin: 60,
            dots: true,
            nav: true,
            navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            items: 1,
        });
    }
});