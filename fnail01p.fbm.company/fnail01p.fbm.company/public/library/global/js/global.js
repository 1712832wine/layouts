/* JS Global */
const stackBottomRightModal = {
    dir1: "up",
    dir2: "left",
    firstpos1: 25,
    firstpos2: 25,
    push: "bottom",
};
let call_notify_object = {};

function callNotify(title_msg, msg, type_notify, delay, remove, type) {
    type_notify = type_notify ? type_notify : "error";
    delay = delay ? +delay : 3000;
    remove = (typeof remove == 'undefined' || remove) ? true : false;

    let icon = "";
    if (type_notify == "error" || type_notify == "notice") {
        icon = "fa fa-exclamation-circle";
    } else if (type_notify == "success") {
        icon = "fa fa-check-circle";
    }

    if (remove && typeof call_notify_object.remove === 'function') {
        call_notify_object.remove();
    }

    let option = {
        title: title_msg,
        text: msg,
        type: type_notify,
        icon: icon,

        closer: true,
        closerHover: true,
        sticker: false,
        stickerHover: false,
        labels: {close: 'Close', stick: 'Stick', unstick: 'Unstick'},
        classes: {closer: 'closer', pinUp: 'pinUp', pinDown: 'pinDown'},

        remove: true,
        destroy: true,
        mouseReset: true,
        delay: delay,
    };

    if (!type) {
        option.addclass = 'alert-with-icon stack-bottomright';
        option.stack = stackBottomRightModal;
    } else {
        option.addclass = 'alert-with-icon';
    }

    call_notify_object = new PNotify(option);

    return call_notify_object;
}

function call_notify(title_msg, msg, type_notify) {
    callNotify(title_msg, msg, type_notify, 0, 1, 1);
}

function showValidateMsg(objThis, msg) {
    if (!msg) {
        msg = objThis.attr('data-validation-message');
    }
    objThis.addClass('error');
    objThis.parent().append(`<div class="form-tooltip-error"><ul><li>${msg}</li></ul></div>`);
}

function clearValidateMsg(objThis) {
    objThis.removeClass("error");
    objThis.parent().find('.form-tooltip-error').remove();
}

function clearAllValidateMsg(objForm) {
    objForm.find('.error').removeClass('error');
    objForm.find('.form-tooltip-error').remove();
}

function change_content(elemenThis, elemenTo) {
    $(elemenTo).html($(elemenThis).val());
}

function check_enter_number(evt, onthis) {
    if (isNaN(onthis.value + "" + String.fromCharCode(evt.charCode))) {
        return false;
    }
}

function redirectUrl(url, target) {
    /*Check target*/
    if (typeof target == 'undefined') {
        target = '_self';
    }

    /*append element*/
    let redirect_url = 'redirect_url_' + new Date().getTime();
    $('body').append('<div style="display:none;"><a class="' + redirect_url + '" title="redirect" target="' + target + '">&nbsp;</a></div>');

    /*Call event*/
    let redirect = $('.' + redirect_url);
    redirect.attr('href', url);
    redirect.attr('onclick', "document.location.replace('" + url + "'); return false;");
    redirect.trigger('click');
}

function scrollJumpto(jumpto, headerfixed, redirect) {
    /*Check exits element for jumpto*/
    if ($(jumpto).length > 0) {
        /*Calculator position and call jumpto with effect*/
        jumpto = $(jumpto).offset().top;
        headerfixed = ($(headerfixed).length > 0) ? $(headerfixed).height() : 0;

        $('html, body').animate({
            scrollTop: parseInt(jumpto - (headerfixed + 15)) + 'px'
        }, 1000, 'swing');
    }
    /*Check redirect if not exits element for jumpto*/
    else if (redirect) {
        redirectUrl(redirect);
        return false;
    } else {
        console.log(jumpto + ' Not found.');
    }
}

function initEventScrollJumto() {
    $('.scroll_jumpto').click(function (event) {
        event.preventDefault();

        /*input elements*/
        let jumpto = $(this).data('jumpto');
        let headerfixed = $(this).data('headerfixed');
        let redirect = $(this).data('redirect');

        if (headerfixed === '.freeze-header') {
            headerfixed = window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile';
        }

        /*call fumpto*/
        scrollJumpto(jumpto, headerfixed, redirect);
    });
}

function initImageMagnificPopup(elementClass) {
    let groups = {};
    $(elementClass).each(function () {
        let id = $(this).attr('data-group');
        if (!groups[id]) {
            groups[id] = [];
        }
        groups[id].push(this);
    });
    $.each(groups, function () {
        $(this).magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: true,
            gallery: {enabled: true}
        });
    });
}

function load_social(inputs) {
    if (!inputs) {
        console.log('load social missed inputs');
        return false;
    }

    /*calculator width*/
    let social_block_width = $('#social_block_width').width();
    social_block_width = Math.round(social_block_width);

    if (social_block_width > 450) {
        social_block_width = 450;
    }

    if (social_block_width < 180) {
        social_block_width = 180;
    }

    /*facebook fanpage*/
    if (typeof inputs.facebook_embed != 'undefined' && inputs.facebook_embed) {
        let social_block_height = Math.round(social_block_width * (parseInt(inputs.facebook_embed.height) / parseInt(inputs.facebook_embed.width)));
        let social_url = '';
        if (!inputs.facebook_embed.likebox_enable) {
            social_url += 'https://www.facebook.com/plugins/page.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_height;
            social_url += '&small_header=' + (inputs.facebook_embed.small_header ? 'true' : 'false');
            social_url += '&tabs=' + inputs.facebook_embed.tabs;
            social_url += '&show_facepile=' + (inputs.facebook_embed.show_facepile ? 'true' : 'false');
            social_url += '&hide_cover=false&hide_cta=false&adapt_container_width=true';
        } else {
            social_url += 'https://www.facebook.com/plugins/likebox.php?';
            social_url += '&width=' + social_block_width + '&height=' + social_block_width; // If set height then error with likebox
            social_url += '&show_faces=' + (inputs.facebook_embed.likebox_show_faces ? 'true' : 'false');
            social_url += '&stream=' + (inputs.facebook_embed.likebox_stream ? 'true' : 'false');
            social_url += '&header=false';
        }
        social_url += '&href=' + encodeURIComponent(inputs.facebook_embed.id_fanpage);
        social_url += inputs.facebook_embed.appId ? '&appId' + inputs.facebook_embed.appId : '';

        $('#fanpage_fb_container').html('<iframe style="overflow:hidden;max-height:' + social_block_height + 'px" title="Social fanpage" src="' + social_url + '" width="' + social_block_width + '" height="' + social_block_height + '" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>');
    }

    /*google fanpage*/
    if (typeof inputs.google_id_fanpage != 'undefined' && inputs.google_id_fanpage) {
        $('#fanpage_google_container').html('<div class="g-page" data-href="' + inputs.google_id_fanpage + '" data-width="' + social_block_width + '"></div><script src="https://apis.google.com/js/platform.js" async defer><\/script>');
    }

    /*twitter fanpage*/
    $('#fanpage_twitter_container').html(''); // clear content
    if (typeof inputs.twitter_id_fanpage != 'undefined' && inputs.twitter_id_fanpage) {
        inputs.twitter_id_fanpage = inputs.twitter_id_fanpage.split('/');
        for (let i = inputs.twitter_id_fanpage.length - 1; i >= 0; i -= 1) {
            if (inputs.twitter_id_fanpage[i] != '') {
                inputs.twitter_id_fanpage = inputs.twitter_id_fanpage[i];
                break;
            }
        }
        if (typeof twttr != 'undefined') {
            twttr.widgets.createTweet(inputs.twitter_id_fanpage, document.getElementById('fanpage_twitter_container'), {width: social_block_width});
        }
    }
}

function isOnScroll(container, selector, header, boundSubtraction) {
    container = (typeof container == "undefined") ? "" : container;
    selector = (typeof selector == "undefined") ? "" : selector;
    header = (typeof header == "undefined") ? "" : header;

    /*Check exit element*/
    if (!$(container).length || !$(selector).length) {
        return false;
    }

    /*Append element instead*/
    let injectSpace = $('<div />', {
        height: $(selector).outerHeight(true),
        class: 'injectSpace' + new Date().getTime()
    }).insertAfter($(selector));
    injectSpace.hide();

    /*Check scroll*/
    $(window).scroll(function () {
        if ($(container).is_on_scroll(selector, header, boundSubtraction)) {
            injectSpace.show();
        } else {
            injectSpace.hide();
        }
    });
}

function isFreezeHeader(wrapFreezeHeader, flagFreezeHeader, device) {
    let deviceName = device == 'mobile' ? 'mobile' : 'desktop';
    let wrapFreezeHeaderObj = $(wrapFreezeHeader);
    let flagFreezeHeaderObj = $(flagFreezeHeader);

    if (!flagFreezeHeaderObj.hasClass('initializedFreezeHeader') && wrapFreezeHeaderObj.length > 0 && flagFreezeHeaderObj.length > 0) {
        flagFreezeHeaderObj.addClass('initializedFreezeHeader');
        wrapFreezeHeaderObj.addClass(`fixed-freeze ${deviceName}`);

        let insteadFreezeHeaderObj = $(`<div class="instead-flag-freeze-header ${deviceName}"></div>`);
        insteadFreezeHeaderObj.insertBefore(flagFreezeHeaderObj);

        $(window).scroll(function () {
            if (wrapFreezeHeaderObj.is_on_scroll1()) {
                flagFreezeHeaderObj.removeClass(`freeze-header with-bg ${deviceName}`);
                insteadFreezeHeaderObj.height('0');
            } else {
                insteadFreezeHeaderObj.height(flagFreezeHeaderObj.outerHeight() + 'px');
                flagFreezeHeaderObj.addClass(`freeze-header with-bg ${deviceName}`);
            }
        });
    }
}

function setActiveMenu(elementMenuMain, elementMenuMobile, inputs) {
    if (!inputs) {
        console.log('set active menu missed inputs');
        return false;
    }
    if (typeof inputs.site == "undefined" || inputs.site == "idx") {
        inputs.site = "";
    }
    if (typeof inputs.site_act == "undefined") {
        inputs.site_act = "";
    }
    if (inputs.site == 'p') {
        inputs.site += '/' + inputs.site_act;
    }

    elementMenuMain = elementMenuMain.split(",");
    for (let j in elementMenuMain) {
        elementMenuMain[j] += ` > li > a[href="/${inputs.site}"]`;
    }
    elementMenuMain = elementMenuMain.join(',');

    elementMenuMobile = elementMenuMobile.split(",");
    for (let j in elementMenuMobile) {
        elementMenuMobile[j] += ` > li > a[href="/${inputs.site}"]`;
    }
    elementMenuMobile = elementMenuMobile.join(',');

    $(`${elementMenuMain}, ${elementMenuMobile}`).addClass('active').parent().addClass('active');
}

function initMenuMobile(menu, container, widthScreen, options) {
    if ($(menu).length <= 0) {
        return;
    }

    let defaults = {
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>",
        meanExpand: "+",
        meanContract: "-",
    };
    options = $.extend(defaults, options);

    if (typeof options.meanRevealPosition == "undefined") {
        options.meanRevealPosition = "right";
    }
    if (typeof options.meanMenuOpen == "undefined") {
        options.meanMenuOpen = "<span></span>";
    }
    if (typeof options.meanExpand == "undefined") {
        options.meanExpand = "+";
    }
    if (typeof options.meanContract == "undefined") {
        options.meanExpand = "-";
    }

    $(menu).meanmenu({
        meanMenuContainer: container,
        meanScreenWidth: widthScreen * 1,
        meanRevealPosition: options.meanRevealPosition,
        meanMenuOpen: options.meanMenuOpen,
        meanExpand: options.meanExpand,
        meanContract: options.meanContract,
    });
}

function initMenuMobile2(menu, container, wrapContainer, widthScreen, options) {
    if ($(menu).length <= 0) {
        return;
    }

    let defaults = {
        translatemenuBarHtml: '<span></span>',
        translatemenuBarPosition: 'right',

        translatemenuNavPosition: 'right',
        translatemenuNavExpand: '<span>></span>',
        translatemenuNavContract: '<span>< BACK</span>',
        translatemenuNavMinWidth: 240,
    };
    options = $.extend(defaults, options);

    if (typeof options.translatemenuBarHtml == "undefined") {
        options.translatemenuBarHtml = '<span></span>';
    }
    if (typeof options.translatemenuBarPosition == "undefined") {
        options.translatemenuBarPosition = 'right';
    }
    if (typeof options.translatemenuNavPosition == "undefined") {
        options.translatemenuNavPosition = 'right';
    }
    if (typeof options.translatemenuNavExpand == "undefined") {
        options.translatemenuNavExpand = '<span>></span>';
    }
    if (typeof options.translatemenuNavContract == "undefined") {
        options.translatemenuNavPosition = '<span>< BACK</span>';
    }
    if (typeof options.translatemenuNavMinWidth == "undefined") {
        options.translatemenuNavMinWidth = 240;
    }

    $(menu).translatemenu({
        translatemenuContainer: container,
        translatemenuWrapContainer: wrapContainer,
        translatemenuScreenWidth: widthScreen * 1,

        translatemenuBarHtml: options.translatemenuBarHtml,
        translatemenuBarPosition: options.translatemenuBarPosition,
        translatemenuNavPosition: options.translatemenuNavPosition,
        translatemenuNavExpand: options.translatemenuNavExpand,
        translatemenuNavContract: options.translatemenuNavContract,
        translatemenuNavMinWidth: options.translatemenuNavMinWidth,
    });
}

function initSliderHome(slider, sliderFixedHeight, sliderItem, selector, selectorOption) {
    if ($(sliderItem).length <= 0) {
        return;
    }

    /*calculator width height slider*/
    $(selector).show();

    /*width-height*/
    let width = '100%';
    let height = $(selector).find('.fixed').height();
    if (height <= 0) {
        let heights = [];
        $(selector).each(function () {
            heights.push($(this).find('img').height());
        });

        if (heights.length > 0) {
            height = Math.min.apply(Math, heights);
        }
    }
    height = (height > 0) ? height : 450;

    $(selector).hide();

    /*init slider and remove other slider*/
    let sliderOption = {};
    sliderOption.autoHeight = $(selector).find('.fixed').height() ? false : true;

    let sliderOptionObj = $(selector).find(selectorOption ? selectorOption : '#slider-option');
    sliderOption.autoplay = sliderOptionObj.attr('data-autoplay');
    sliderOption.autoplay = sliderOption.autoplay == 'true' ? true : false; // Boolean

    sliderOption.autoplayDelay = sliderOptionObj.attr('data-autoplayDelay') * 1;
    sliderOption.autoplayDelay = sliderOption.autoplayDelay >= 1000 ? sliderOption.autoplayDelay : 5000; // milliseconds

    let sliderInit = sliderOption.autoHeight ? slider : sliderFixedHeight;
    $(sliderInit).show().sliderPro({
        width: width,
        height: height,
        autoHeight: sliderOption.autoHeight,
        responsive: sliderOption.autoHeight,
        centerImage: false,
        autoScaleLayers: false,

        arrows: true,
        fade: true,
        buttons: true,
        thumbnailArrows: true,

        autoplay: sliderOption.autoplay,
        autoplayDelay: sliderOption.autoplayDelay,
    });
    if (sliderInit === slider) {
        $(sliderFixedHeight).remove();
    } else {
        $(slider).remove();
    }
}

function initScrollToTop(scrollToTop) {
    if ($(scrollToTop).length <= 0) {
        return;
    }

    $(window).on('scroll', function () {
        let ScrollTop = $(scrollToTop);
        if ($(window).scrollTop() > 1000) {
            ScrollTop.fadeIn(1000);
        } else {
            ScrollTop.fadeOut(1000);
        }
    });

    $(document).on('click', `${scrollToTop}`, function () {
        $("html,body").animate({
            scrollTop: 0
        }, 500);
    });
}

/*
* ids: '' | 'id+id+id'
* */
function getMenuCategory(ids = [], container = '', callback = function (data) {}) {
    let maskLoadingObj = $('<div class="mask_booking" style="position: absolute; height: 100%; width: 100%; top: 0; left: 0; background:rgba(0,0,0,0.5);text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>');
    $.ajax({
        type: 'get',
        url: '/service/get_category/id+' + (ids.constructor === Array ? ids.join('+') : ids),
        dataType: 'Json',
        beforeSend: function () {
            $(container).append(maskLoadingObj);
        },
        success: function (obj) {
            if (typeof callback === 'function') {
                callback(obj);
            }
        },
        complete: function () {
            maskLoadingObj.remove();
        }
    });
}