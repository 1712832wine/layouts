/*
* Translate Menu
* Version 1.0.0
* By ThamLV
*/
(function ($) {
    "use strict";
    $.fn.translatemenu = function (options) {
        // Settings
        let defaults = {
			translatemenuScreenWidth: 480, // Pixel
            translatemenuContainer: 'body',
			translatemenuWrapContainer: '',

            translatemenuBarHtml: '<span></span>',
            translatemenuBarPosition: 'right', // left | center | right

            translatemenuNavPosition: 'right', // left | right
            translatemenuNavExpand: '<span>></span>',
            translatemenuNavContract: '<span>< BACK</span>',
			translatemenuNavMinWidth: 240, // Pixel
        };
        options = $.extend(defaults, options);

        // Validate
		options.translatemenuBarPosition = ['left', 'center', 'right'].indexOf(options.translatemenuBarPosition) >= 0 ? options.translatemenuBarPosition : 'right';
		options.translatemenuNavPosition = ['left', 'right'].indexOf(options.translatemenuNavPosition) >= 0 ? options.translatemenuNavPosition : 'right';

        // Device Is Mobile/Tablet
        let isMobile = ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/Android/i)) || (navigator.userAgent.match(/Blackberry/i)) || (navigator.userAgent.match(/Windows Phone/i)));

        // Init
		let translatemenuScreenWidth = options.translatemenuScreenWidth;
		let translatemenuContainer = options.translatemenuContainer;
		let translatemenuWrapContainer = options.translatemenuWrapContainer;

		let translatemenuBarHtml = options.translatemenuBarHtml;
		let translatemenuBarPosition = options.translatemenuBarPosition;

		let translatemenuNavPosition = options.translatemenuNavPosition;
		let translatemenuNavExpand = options.translatemenuNavExpand;
		let translatemenuNavContract = options.translatemenuNavContract;
		let translatemenuNavMinWidth = options.translatemenuNavMinWidth;

        return this.each(function () {
            let translatemenu = jQuery(this);

            let initTranslatemenu = function(){
            	// Already Init
				let initialized = translatemenu.hasClass('translatemenuInitialized');

				// Client Width
				let currentWidth = window.innerWidth || document.documentElement.clientWidth;

				// Screen width
				if (initialized || currentWidth > translatemenuScreenWidth) {
					if (currentWidth > translatemenuScreenWidth) {
						jQuery('.translate-bar, .translate-nav, .translate-overlay').remove();
						jQuery('.translate-nav-container, .translate-bar-container').removeClass('translate-nav-container translate-bar-container');
						jQuery(translatemenuWrapContainer).css({
							'-webkit-transform' : '',
							'-moz-transform'    : '',
							'-ms-transform'     : '',
							'-o-transform'      : '',
							'transform'         : ''
						});
						translatemenu.removeClass('translatemenuInitialized').css({'display': '',});
					}

					return true;
				}

				// Nav Width
				translatemenu.addClass('translatemenuInitialized').css({'display': 'inline-block',});
				let translatemenuNavWidth = translatemenu.outerWidth(false) + 100;
					translatemenuNavWidth = translatemenuNavWidth > translatemenuNavMinWidth ? translatemenuNavWidth : translatemenuNavMinWidth;
					translatemenuNavWidth = translatemenuNavWidth < (currentWidth - 30) ? translatemenuNavWidth : (currentWidth - 30);

				// Init Bar
				jQuery(translatemenuContainer).addClass(`translate-bar-container`)
					.prepend(`<div class="translate-bar ${translatemenuBarPosition}"><nav class="translate-reveal close-reveal">${translatemenuBarHtml}</nav>`);

				// Init Nav
				translatemenu.hide();
				let translatemenuNavTransformWidth = translatemenuNavPosition === 'left' ? - translatemenuNavWidth : translatemenuNavWidth;
				jQuery('body').addClass('translate-nav-container')
					.prepend(`<div class="translate-nav ${translatemenuNavPosition}" style="width: ${translatemenuNavWidth}px;-webkit-transform: translate(${translatemenuNavTransformWidth}px, 0px); -ms-transform: translate(${translatemenuNavTransformWidth}px, 0px); -o-transform: translate(${translatemenuNavTransformWidth}, 0px); transform: translate(${translatemenuNavTransformWidth}px, 0px);"></div>`);

				// Html Nav
				let translateMenuHtmlList = [];
				let translateMenuHtmlCnt = 0;
				let generalTranslateMenu = function(htmlInput, _ulId = '', _ulIdBack = '') {
					let _html = `<ul class="translate-menu" id="${_ulId}">`;

					if (_ulIdBack) {
						_html += `<li class="translate-contract" data-id="${_ulIdBack}">${translatemenuNavContract}</li>`;
					}

					jQuery(htmlInput).find('>li').each(function () {
						let _this = jQuery(this);
						let _li = jQuery('<li>' + _this.html() + '</li>');
						let _ul = _this.find('>ul').first();
						let _class = '';
						let _expand = '';

						if (_ul.length > 0) {
							translateMenuHtmlCnt++; // Increase CNT

							let _ulIdNew = `menu-${translateMenuHtmlCnt}`;

							_class = 'class="translate-submenu"';
							_expand = `<div class="translate-expand" data-id="${_ulIdNew}">${translatemenuNavExpand}</div>`;

							generalTranslateMenu(_ul, _ulIdNew, _ulId);
							_li.find('>ul').remove();
						}
						_html += `<li ${_class}>${_expand}` + _li.html() + '</li>';
					});
					_html += '</ul>';

					translateMenuHtmlList.push(_html);
				};
				generalTranslateMenu(translatemenu.find('ul').first(), `menu-${translateMenuHtmlCnt}`);

				jQuery.each(translateMenuHtmlList, function (index, value) {
					jQuery('.translate-nav').prepend(value);
				});

				jQuery('.translate-menu').hide();
				jQuery('.translate-menu#menu-0').show();

				jQuery('.translate-nav').on('click', '.translate-expand', function () {
					jQuery(this).closest('.translate-menu').css({
						'-webkit-animation' : 'slide-out-left 0.2s',
						'-moz-animation'    : 'slide-out-left 0.2s',
						'-ms-animation'     : 'slide-out-left 0.2s',
						'-o-animation'      : 'slide-out-left 0.2s',
						'animation'         : 'slide-out-left 0.2s'
					});
					jQuery('#' + jQuery(this).data('id')).css({
						'-webkit-animation' : 'slide-in-left 0.2s',
						'-moz-animation'    : 'slide-in-left 0.2s',
						'-ms-animation'     : 'slide-in-left 0.2s',
						'-o-animation'      : 'slide-in-left 0.2s',
						'animation'         : 'slide-in-left 0.2s'
					});
				});

				jQuery('.translate-nav').on('click', '.translate-contract', function () {
					jQuery(this).closest('.translate-menu').css({
						'-webkit-animation' : 'slide-in-right 0.2s',
						'-moz-animation'    : 'slide-in-right 0.2s',
						'-ms-animation'     : 'slide-in-right 0.2s',
						'-o-animation'      : 'slide-in-right 0.2s',
						'animation'         : 'slide-in-right 0.2s'
					});
					jQuery('#' + jQuery(this).data('id')).css({
						'-webkit-animation' : 'slide-out-right 0.2s',
						'-moz-animation'    : 'slide-out-left 0.2s',
						'-ms-animation'     : 'slide-out-right 0.2s',
						'-o-animation'      : 'slide-out-right 0.2s',
						'animation'         : 'slide-out-right 0.2s'
					});
				});

				jQuery('.translate-nav').on('click', '.translate-expand, .translate-contract', function () {
					jQuery(this).closest('.translate-menu').hide();
					jQuery('#' + jQuery(this).data('id')).show();
				});

				// Init Wrapper
				jQuery(translatemenuWrapContainer).addClass('translate-wrap').prepend('<div class="translate-overlay"></div>');

				// Init Events
				let processTranslateMenu = function (showTranslateMenu) {
					let translateNav = jQuery('.translate-nav');
					let translatemenuTransformWidth = translateNav.outerWidth(false);
					if (showTranslateMenu) {
						jQuery('.translate-overlay').show();
						jQuery('.translate-reveal').removeClass('close-reveal').addClass('open-reveal');

						translateNav.css({
							'-webkit-transform' : '',
							'-moz-transform'    : '',
							'-ms-transform'     : '',
							'-o-transform'      : '',
							'transform'         : ''
						});

						translatemenuTransformWidth = translateNav.hasClass('left') ? translatemenuTransformWidth : -translatemenuTransformWidth;
						jQuery('.translate-wrap').css({
							'-webkit-transform' : `translate(${translatemenuTransformWidth}px, 0)`,
							'-moz-transform'    : `translate(${translatemenuTransformWidth}px, 0)`,
							'-ms-transform'     : `translate(${translatemenuTransformWidth}px, 0)`,
							'-o-transform'      : `translate(${translatemenuTransformWidth}px, 0)`,
							'transform'         : `translate(${translatemenuTransformWidth}px, 0)`
						});
					} else {
						jQuery('.translate-reveal').removeClass('open-reveal').addClass('close-reveal');
						jQuery('.translate-overlay').hide();

						translatemenuTransformWidth = translateNav.hasClass('left') ? -translatemenuTransformWidth : translatemenuTransformWidth;
						translateNav.css({
							'-webkit-transform' : `translate(${translatemenuTransformWidth}px, 0)`,
							'-moz-transform'    : `translate(${translatemenuTransformWidth}px, 0)`,
							'-ms-transform'     : `translate(${translatemenuTransformWidth}px, 0)`,
							'-o-transform'      : `translate(${translatemenuTransformWidth}px, 0)`,
							'transform'         : `translate(${translatemenuTransformWidth}px, 0)`
						});

						jQuery('.translate-wrap').css({
							'-webkit-transform' : '',
							'-moz-transform'    : '',
							'-ms-transform'     : '',
							'-o-transform'      : '',
							'transform'         : ''
						});
					}
				};

				jQuery('body').on('click', '.translate-reveal.close-reveal', function () {
					processTranslateMenu(true);
				});

				jQuery('body').on('click', '.translate-overlay, .translate-reveal.open-reveal', function () {
					processTranslateMenu(false);
				});
			};

			jQuery(window).resize(function () {
				initTranslatemenu();
			});
			initTranslatemenu();
		});
    };
})(jQuery);
