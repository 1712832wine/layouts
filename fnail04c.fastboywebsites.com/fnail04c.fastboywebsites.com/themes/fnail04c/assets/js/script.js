$(document).ready(function() {
    new WOW().init();


	/*////////////// MY SLIDER ///////////////*/
    $( '#my-slider' ).sliderPro({
        width: 1976,
        height:800,
        arrows: true,
        fade: true,
        autoHeight:true,
        centerImage:true,
        autoScaleLayers: false,

        buttons: false,
        thumbnailArrows: true,
        autoplay: true,
        slideSpeed : 300,
        breakpoints: {
            768: {
                width: 1024,
                height:600,
                arrows: true,
                fade: true,
                autoHeight:true,
                centerImage:true,
                autoScaleLayers: false,
                forceSize: 'fullWidth',
                buttons: false,
                thumbnailArrows: false,
                autoplay: true,
                slideSpeed : 300,
            }
        },
    });

  

    var h_doc = $('body').height();
    var h_screen = $(window).height();
    var w_screen = $(window).width();
    var h_main = $('main').height();
    if(w_screen<=768){
        h_screen = h_screen-70;
    }
    $('.section-home').css('height', (h_screen)+'px');

    
    $(function(){
        $(window).scroll(function(){
        
            if($(this).scrollTop()>10){
                $('.nav-left').addClass('stickUp');
            }else{
                $('.nav-left').removeClass('stickUp');
            }
        });
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > h_screen) {
            $('.nav-left').addClass('stickUp');
        }
    });


    if(h_screen>h_doc) {
        var newH = h_screen-h_doc+h_main;
        $('main').css('height', (newH)+'px');
    }








	/*////////////// MOBILE NAV ///////////////*/
	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: "990",
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>"
    });
    

    /*////////////// GALLERY ///////////////*/
    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          gallery: { enabled:true }
      })
      
    });


    var hgroups = {};
    $('.gla-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!hgroups[id]) {
        hgroups[id] = [];
      }       
      hgroups[id].push( this );
    });


    $.each(hgroups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: false,
          gallery: { enabled:true }
      })
      
    });
    $('.popup-youtube').magnificPopup({

        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: true,
        fixedContentPos: false
    });



    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 600) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });
        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    $(".list-client").owlCarousel({
        loop:true,
        nav:false,
        margin:30,
        dots:false,
        smartSpeed:1500,
        responsive:{
            0:{items:1},
            600:{ items:2},
            1000:{ items:3}
        }

    });

    $(".blog-client.owl-carousel").owlCarousel({
        loop:true,
        nav:false,
        margin:30,
        dots:false,
        smartSpeed:1000,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        responsive:{
            0:{items:1},
            600:{ items:2},
            1000:{ items:3}
        }

    });


})