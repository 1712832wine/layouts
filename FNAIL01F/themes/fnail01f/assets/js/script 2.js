$(document).ready(function() {
    new WOW().init();
    
    
	/*////////////// MY SLIDER ///////////////*/
    $( '#my-slider' ).sliderPro({
        fade: true,
        width: '100%', 
        height:500,
        arrows: true,
        buttons: false,  
        autoplayDelay:3000,
        autoHeight:true,
        fadeDuration:3000,
    });
    $('.list_gallery_home').owlCarousel({
        /*loop:true,*/
        margin:0,
        nav:true,
        navContainerClass: 'carousel-nav-btn-gc',
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        navClass: ['carousel-nav-left','carousel-nav-right'],
        responsive:{
            0:{items:1,},
            576:{items:2,},
            768:{items:3,},
            992:{items:4,},
            1200:{items:5,},
        }
    });
    
	/*/!*!////////////// MOBILE NAV ///////////////!*!/
	$('.mobile-menu nav').meanmenu({
        meanMenuContainer: '.menu_mobile_v1',
        meanScreenWidth: "990",
        meanRevealPosition: "right",
        meanMenuOpen: "<span></span>"
    });*/
    

    /*////////////// GALLERY ///////////////*/
    var groups = {};
    $('.gallery-item').each(function() {
      var id = parseInt($(this).attr('data-group'), 10);      
      if(!groups[id]) {
        groups[id] = [];
      }       
      groups[id].push( this );
    });


    $.each(groups, function() {
      
      $(this).magnificPopup({
          type: 'image',
          closeOnContentClick: true,
          closeBtnInside: true,
          gallery: { enabled:true }
      })
      
    });
    /*////////////// BOOKING ///////////////*/
    $(document).ready(function() {
      //$('#datetimepicker_v1').datetimepicker();
    });
    // CONFIRM BOOKING
    $(document).ready(function(){
        $(".databooktime").on("click",".open_booking", function(){
            var hours = $(this).attr("valhours");
            $.magnificPopup.open({
                type: 'inline',
                midClick: true,
                items: {
                  src: '#open_booking'
                },
                callbacks: {
                    beforeOpen: function() {
                        if($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                        $("input[name='booking_hours']").val(hours);


                    }
                }
            });

            return false;
        });

        $(".btn_cancel").click(function(){
            $.magnificPopup.close();
            
        });
    });
    // END CONFIRM BOOKING
    // FLEX LABEL IN PAYMENT 
   // $('.fl-flex-label').flexLabel();

   $(function () {
        $('.back-to-top').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    $("body").append('<p id="back-top"> <a href="#top"><i class="fa fa-chevron-up" aria-hidden="true"></i></a> </p>');
    $("#back-top").hide();
    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
    $('#back-top a').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    
    
});