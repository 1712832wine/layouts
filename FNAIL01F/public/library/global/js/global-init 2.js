/* JS Global Init*/
(function ($) {
    'use strict';

    /*IS ON SCROLL*/
    $.fn.is_on_scroll = function(selector, header, boundSubtraction ) {
        /*Calculate viewport*/
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /*Calculate bounds*/
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}

        if ( $(boundSubtraction).length > 0 ) {
            let boundSubtractionHeight = $(boundSubtraction).outerHeight(true);
            let boundMaxWidth = $(boundSubtraction).attr('bound-maxwidth');
            boundMaxWidth = (typeof boundMaxWidth == "undefined") ? 0 : boundMaxWidth;
            if ( boundMaxWidth > 0 && window.matchMedia('(max-width: ' + boundMaxWidth + 'px)').matches != true ) {
                boundSubtractionHeight = 0;
            }
            bounds.top = bounds.top + boundSubtractionHeight;
        }
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        let boundsSelectorObj = $(selector).parent();
        let boundsSelector = boundsSelectorObj.offset();
        boundsSelector.right = boundsSelector.left + boundsSelectorObj.outerWidth();
        boundsSelector.bottom = boundsSelector.top + boundsSelectorObj.outerHeight();

        /*Calculate header fixed*/
        let headerHeight = 0;
        if ( $(header).length > 0 ) {
            headerHeight = $(header).outerHeight(true);

            /*Check fixed*/
            let checkFixed = $(header).attr('checkfixed');
            checkFixed = (typeof checkFixed == "undefined") ? 'false' : checkFixed;
            if ( checkFixed === "true" && $(header).css('position') != 'fixed' ){
                headerHeight = 0;
            }

            /*Check max width*/
            let maxWidth = $(header).attr('header-maxwidth');
            maxWidth = ( typeof maxWidth == undefined ) ? 0 : maxWidth;
            if ( maxWidth > 0 && window.matchMedia('(max-width: ' + maxWidth + 'px)').matches != true ) {
                headerHeight = 0;
            }
        }

        if ( viewport.top >= ( bounds.top - headerHeight ) && viewport.top <= ( bounds.bottom - headerHeight ) ) {
            $(selector).css({
                'position': 'fixed',
                'top': headerHeight + 'px',
                'right': (viewport.right - boundsSelector.right) + 'px',
                'z-index': '1001',
            });
            return true;
        } else {
            $(selector).css({
                'position': '',
            });
            return false;
        }
    }

    $.fn.is_on_scroll1 = function() {
        /* Not included margin, padding of window */
        let win = $(window);
        let viewport = {
            top : win.scrollTop(),
            left : win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        /* Not included margin of this element: same container */
        let bounds = this.offset();
        if ( typeof bounds == 'undefined' ) {return false;}
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        if ( bounds.top >= viewport.top && bounds.bottom <= viewport.bottom ) {
            return true;
        } else {
            return false;
        }
    };

    /* ANCHOR LINK */
    $('[href^="#"]').on("click", function (event) {
        let _h = $(this).attr('href');
        let _hsplit = _h.substr(1, _h.length);
        if ( _hsplit != 'open_booking' ) {
            event.preventDefault();
            scrollJumpto(_h, window.matchMedia('(min-width: 992px)').matches ? '.fixed-freeze.desktop' : '.fixed-freeze.mobile');
        }
    });

    /*
    * MAX-LENGTH
    * */
    let maxlengthCnt = 0;
    $('[maxlength]').each(function () {
        let _self = $(this);
        if( parseInt(_self.attr('maxlength')) > 0 ){
            _self.attr('data-maxlength-error', 'maxlength-error-' + maxlengthCnt);
            _self.after('<div id="maxlength-error-'+ maxlengthCnt+'" class="maxlength-error"></div>');
            _self.bind('input propertychange', function() {
                let _this = $(this);
                let maxLength = parseInt(_this.attr('maxlength'));
                let containerError = $('#'+_this.attr('data-maxlength-error'));
                let text = _this.val();
                if( text.length > maxLength-1 ){
                    _this.val(text.substring(0, maxLength-1));
                    containerError.html('Max Length allowed is '+ (maxLength -1)  + ' character');
                } else {
                    containerError.html('');
                }
            });
            maxlengthCnt++;
        }
    });

    /* REMOVE VALIDATE MSG */
    $('body').on('select2:close, change, focus', 'select, input', function (e) {
        clearValidateMsg($(this));
    });

    /*MAGIC POPUP*/
    initImageMagnificPopup('.m-magnific-popup');

    /*MASK AND INIT*/
    if ( typeof webFormat !== 'undefined') {
        $('.inputPhone').mask(webFormat.phoneFormat.replace(/\d/g, '0'), {placeholder: webFormat.phoneFormat.replace(/\d/g, '_')});

        let inputDate = $('.inputDate');
        inputDate.mask(webFormat.dateFormat.replace(/[^\d\/]/g, '0'), {placeholder: webFormat.dateFormat});
        inputDate.datetimepicker({
            format: webFormat.dateFormat,
        });
    } else {
        $('.inputPhone').mask(phoneFormat.replace(/\d/g, '0'), {placeholder: phoneFormat.replace(/\d/g, '_')});

        let inputDate = $('.inputDate');
        inputDate.mask(dateFormatBooking.replace(/[^\d\/]/g, '0'), {placeholder: dateFormatBooking});
        inputDate.datetimepicker({
            format: dateFormatBooking,
        });
    }
})(jQuery);

$(document).ready(function () {

    /*SCROLL TO TP*/
    initScrollToTop('.back-to-top');

    /*SCROLL OR JUMP*/
    initEventScrollJumto();

    /*
    * SOCIAL FAN PAGE
    * When resize then reload fanpage
    * Firing resize event only when resizing is finished
    */
    let socialInputs = {
        facebook_embed: facebook_embed,
        google_id_fanpage: google_id_fanpage,
        twitter_id_fanpage: twitter_id_fanpage,
    };
    $(window).load(function() {
        load_social(socialInputs);
        $(window).on('resize', function () {
            clearTimeout(window.resizedFinished);
            window.resizedFinished = setTimeout(function () {
                load_social(socialInputs);
            }, 250);
        });
    });
});