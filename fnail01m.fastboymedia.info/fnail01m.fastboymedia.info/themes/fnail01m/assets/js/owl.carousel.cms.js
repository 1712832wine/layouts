(function ($) {
    "use strict";
    $(document).ready(function () {
        var cmscarousel = {
            "cms-carousel": {
                "margin": 10,
                "loop": "true",
                "mouseDrag": "true",
                "nav": "false",
                "dots": "false",
                "autoplay": "true",
                "autoplayTimeout": 5000,
                "smartSpeed": 1000,
                "autoplayHoverPause": "true",
                "navText": ["<i class=\"fa fa-arrow-left\"><\/i>", "<i class=\"fa fa-arrow-right\"><\/i>"],
                "dotscontainer": "cms-carousel .cms-dots",
                "responsive": {
                    "0": {"items": 1, "nav": "true",},
                    "768": {"items": 2, "nav": "true",},
                    "992": {"items": 4},
                    "1200": {"items": 5}
                }
            }
        };
        $(".cms-carousel").each(function () {
            var $this = $(this), slide_id = $this.attr('id'), slider_settings = cmscarousel[slide_id];
            if ($this.attr('data-sliderSettings')) {
                slider_settings = jQuery.parseJSON($this.attr('data-sliderSettings'));
            } else {
                slider_settings.margin = parseInt(slider_settings.margin);
                slider_settings.loop = (slider_settings.loop === "true");
                slider_settings.mouseDrag = (slider_settings.mouseDrag === "true");
                slider_settings.nav = (slider_settings.nav === "true");
                slider_settings.dots = (slider_settings.dots === "true");
                slider_settings.autoplay = (slider_settings.autoplay === "true");
                slider_settings.autoplayTimeout = parseInt(slider_settings.autoplayTimeout);
                slider_settings.autoplayHoverPause = (slider_settings.autoplayHoverPause === "true");
                slider_settings.smartSpeed = parseInt(slider_settings.smartSpeed);
                if ($('.cms-dot-container' + slide_id).length) {
                    slider_settings.dotsContainer = '.cms-dot-container' + slide_id;
                    slider_settings.dotsEach = true;
                }
            }

            /*
            * Extends Slider Option
            * */
            let sliderOptionObj = $('#slider-option');
            let sliderOption = {};
            sliderOption.loop = sliderOptionObj.attr('data-loop');
            sliderOption.autoplay = sliderOptionObj.attr('data-autoplay');
            sliderOption.autoplayTimeout = sliderOptionObj.attr('data-autoplayTimeout');
            sliderOption.autoplayHoverPause = sliderOptionObj.attr('data-autoplayHoverPause');
            sliderOption.responsive = sliderOptionObj.attr('data-responsive');

            if (typeof sliderOption.loop != 'undefined') {
                slider_settings.loop = sliderOption.loop == 'false' ? false : true;
            }

            if (typeof sliderOption.autoplay != 'undefined') {
                slider_settings.autoplay = sliderOption.autoplay == 'false' ? false : true;
            }

            if (typeof sliderOption.autoplayTimeout != 'undefined') {
                slider_settings.autoplayTimeout = sliderOption.autoplayTimeout >= 1000 ? sliderOption.autoplayTimeout : 5000;
            }

            if (typeof sliderOption.autoplayHoverPause != 'undefined') {
                slider_settings.autoplayHoverPause = sliderOption.autoplayHoverPause == 'false' ? false : true;
            }

            if (typeof sliderOption.responsive != 'undefined') {
                let responsive = sliderOption.responsive.split('|');
                sliderOption.responsive = {};
                for (let i in responsive) {
                    responsive[i] = responsive[i].split(',');
                    sliderOption.responsive[responsive[i][0]] = {};
                    if (responsive[i][1]) {
                        sliderOption.responsive[responsive[i][0]].items = parseInt(responsive[i][1]);
                    }
                    if (responsive[i][2]) {
                        sliderOption.responsive[responsive[i][0]].nav = responsive[i][2] == 'false' ? false : true;
                    }
                }
                slider_settings.responsive = sliderOption.responsive;
            }

            $this.owlCarousel(slider_settings);
        });
    });
})(jQuery)